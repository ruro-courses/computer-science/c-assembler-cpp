#include<stdio.h>

int operation(void) {
    int number;
    char operator;
    if (scanf("%d", &number)) {
        return number;
    } else {
        while (!scanf(" %c", &operator));
        switch (operator) {
            case '*':
                return operation() * operation();
            case '/':
                return operation() / operation();
            default:
                return -1;
        }
    }
}

int main(void) {
    printf("%d", operation());
}