#include<stdio.h>
#include <stdbool.h>

bool isPrime(int a) {
    int divisor = 3;
    int composite = 0;
    while (true) {
        composite = a / divisor;
        if (composite < divisor) { return true; }
        //We are searching for a = composite * divisor. Divisor is increasing, Composite decreasing.
        //If they cross, it means we are going over the same values a second time.
        if (a == divisor * composite) { return false; }
        //Division was without remainder
        divisor += 2;
    }
}

int main(void) {
    int N;
    scanf("%d", &N);
    if (N <= 2) {
        printf("2");
        return 0;
    }
    N += !(N % 2);
    while (!isPrime(N)) { N += 2; }
    printf("%d", N);
}