#include<stdio.h>

int med(int a, int b, int c) {
    return (a > b) ? (b > c ? b : (a > c ? c : a)) : (b < c ? b : (a > c ? a : c));
}

int main(void) {
    int i, len, tmp[1000];
    scanf("%d", &len);
    i = 0;
    //read
    while (i - len) {
        scanf("%d", tmp + i);
        i++;
    }
    while ((len -= 2) + 1) {
        i = 0;
        //reduce arr by 2 elements
        while (i - len) {
            tmp[i] = med(tmp[i], tmp[i + 1], tmp[i + 2]);
            i++;
        }
    }
    printf("%d", tmp[0]);
}