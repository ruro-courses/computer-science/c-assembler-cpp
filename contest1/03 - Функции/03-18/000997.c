#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


bool checkList[2011] = {0};

int nextInt(void) {
    int N;
    scanf("%d", &N);
    return N;
}

int intcat(int a, int b) {
    char ca[10], cb[10];
    sprintf(ca, "%d", a);
    sprintf(cb, "%d", b);
    strcat(ca, cb);
    return strtol(ca, NULL, 10);
}

bool stack(int last, int check) {
    int this = last % 2010;
    return (this == check) || (checkList[this] ? false : (checkList[this] = true, stack(intcat(this, this), check)));
}

int main(void) {
    int N = nextInt();
    int A = nextInt();
    printf("%s", stack(N, A) ? "YES" : "NO");
}