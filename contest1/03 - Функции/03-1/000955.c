#include<stdio.h>

int gcd(int a, int b) {
    return b ? gcd(b, a % b) : a;
}

int main(void) {
    int a, b, c, d;
    scanf("%d%d%d%d", &a, &b, &c, &d);
    printf("%d", gcd(gcd(a,b),gcd(c,d)));
}