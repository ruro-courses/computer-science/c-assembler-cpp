#include <stdio.h>
#include <stdbool.h>

int nextInt(void) {
    int N;
    scanf("%d", &N);
    return N;
}

bool stack(void) {
    int x = nextInt();
    int y = nextInt();
    return (x ? stack() && (x == -nextInt() && y == nextInt()) : true);
}

int main(void) {
    nextInt();
    nextInt();
    nextInt();
    printf("%s", stack() ? "Yes" : "No");
}