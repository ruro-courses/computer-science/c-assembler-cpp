#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>


int pos[2] = {0, 0};
int difMax, difMin;
bool tolikWins = false;

void input(void);

void knightMove(void);

void queenMove(void);

void finish(bool isCheat);

void input(void) {
    int newPos[2] = {0, 0};
    int diff[2] = {0, 0};
    tolikWins = !tolikWins; //Flip, whose turn we are on.

    scanf(" %c", (char *) newPos); //Read char.
    if (newPos[0] == 'X') { finish(pos[0] != 7 || pos[1] != 7); } //Char is X, but last pos wasn't H8! BAN!
    if (pos[0] == 7 && pos[1] == 7) { finish(1); } //Last pos WAS H8, but we are still moving! BAN!
    scanf("%d", newPos + 1);
    newPos[0] -= 'A';
    newPos[1] -= 1;

    diff[0] = abs(newPos[0] - pos[0]);
    diff[1] = abs(newPos[1] - pos[1]);
    difMax = diff[0] > diff[1] ? diff[0] : diff[1];
    difMin = diff[0] < diff[1] ? diff[0] : diff[1]; //Find what move we made.

    pos[0] = newPos[0];
    pos[1] = newPos[1]; //Move.
}

void knightMove(void) {
    input();
    if (difMax == 2 && difMin == 1) { queenMove(); } else { finish(1); } //If we didn't move in an L BAN!
}

void queenMove(void) {
    input();
    if (difMax != 0 && (difMax == difMin || difMin == 0)) { knightMove(); } else { finish(1); }
    //If we didn't move EITHER on one axis or on the diagonal BAN! Also if we didn't move BAN!
}

void finish(bool isCheat) {
    if (isCheat) {
        printf("Cheaters");
        exit(0);
    }
    if (tolikWins) { printf("Tolik"); } else { printf("Garik"); }
    exit(0);
}

int main(void) {
    knightMove();
}