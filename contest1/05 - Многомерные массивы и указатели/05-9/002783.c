#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

#define at(ptr, x, y) *(ptr + (x) + (y)*M)

int N, M;

enum {HEALTHY = 0x00, VIRUS = 0x01, VIRUS_DEAD = 0x03};

void infect(unsigned char *ptr)
{
    int i, x, y;
    scanf("%d", &i);
    while(i--)
    {
        scanf("%d %d", &x, &y);
        at(ptr, x-1, y-1) = VIRUS;
    }
}

bool checkLiving(unsigned char *ptr)
{
    for(int x = 0; x < M; x++)
        for(int y = 0; y < N; y++)
            if(at(ptr, x,y) == HEALTHY)
                return true;
    return false;
}

void tick(unsigned char *old, unsigned char *step)
{
    for(int x = 0; x < M; x++)
    {
        for(int y = 0; y < N; y++)
        {
            if(at(old, x, y) == VIRUS)
            {
                at(step, x, y) = VIRUS_DEAD;
                if(x+1<M)
                    at(step, x+1, y) |= VIRUS; //If cell is VIRUS_DEAD 0x01 | 0x03 == 0x03. Still dead.
                if(x>0)
                    at(step, x-1, y) |= VIRUS;
                if(y+1<N)
                    at(step, x, y+1) |= VIRUS;
                if(y>0)
                    at(step, x, y-1) |= VIRUS;
            }
        }
    }
}

int main(void)
{
    unsigned char *ptr, *step;
    scanf("%d%d", &N, &M);
    ptr = calloc(N*M, sizeof(char));
    infect(ptr);
    int i = 0;
    while(checkLiving(ptr)){
        step = malloc(N*M * sizeof(char));
        memcpy(step, ptr, N*M * sizeof(char));
        tick(ptr, step);
        free(ptr);
        ptr = step;
        i++;
    }
    printf("%d", i);
}


