#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>
#define init(A,k) while(!(A = malloc(sizeof(long long) * k)))

long long *matrixMul(long long size, long long *firstMatrix, long long *secondMatrix, long long mod)
{
    long long *ret;
    init(ret, size * size);
    long long i, j, t;
    for(i = 0; i < size; i++)
        for(j = 0; j < size; j++)
        {
            *(ret + i*size + j) = 0;
            for(t = 0; t<size; t++)
            {
                *(ret + i*size + j) = (*(ret + i*size + j) + (*(firstMatrix + i*size + t) **(secondMatrix + t*size + j)) % mod) % mod;
            }
        }
    free(firstMatrix);
    return ret;
}

int main(void)
{
    long long size, mod, i;
    unsigned long long needed;
    scanf("%lld", &size);
    scanf("%llu", &needed);
    scanf("%lld", &mod);
    long long *sequence, *coefficients, *neededCoefficients;
    long long neededElement = 0;
    init(coefficients, size * size);
    init(neededCoefficients, size * size);
    init(sequence, size);
    for(i = 0; i < size; i++)
    {
        scanf("%lld", sequence+i);
        *(sequence+i) %= mod;
    }
    if(needed<=size)
    {
        printf("%lld", *(sequence+needed-1));
        return 0;
    }
    needed-=size;
    for(i = 0; i < size; i++)
    {
        scanf("%lld", coefficients+i);
        *(coefficients+i) %= mod;
    }
    long long sq = size * size;
    for(; i < sq; i++)
        *(coefficients+i) = 0;
    for(i = 1; i<size; i++)
        *(coefficients+i*size+i-1) = 1;
    for(i = 32; !(needed & (1<<i)); i--);
    memcpy(neededCoefficients, coefficients, sizeof(long long) * size * size);
    i--;
    for(; i >= 0; i--)
    {
        neededCoefficients = matrixMul(size, neededCoefficients, neededCoefficients, mod);
        if(needed & (1<<i))
        {
            neededCoefficients = matrixMul(size, neededCoefficients, coefficients, mod);
        }
    }
    for(i = 0; i < size; i++)
        neededElement = (neededElement + (*(neededCoefficients + i)**(sequence + size - i - 1))% mod) % mod;
    printf("%lld", neededElement);
}
