#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

typedef struct
{
    int len;
    int nxt;
    int arr[];
} lenArr;

lenArr *even;

lenArr *init(void)
{
    lenArr *arr;
    while((arr = malloc(sizeof(lenArr) + sizeof(int))) == NULL);
    arr->len = 0;
    arr->nxt = 0;
    return arr;
}

void extend(lenArr **arr)
{
    (*arr)->len *= 2;
    (*arr)->len += 1;
    lenArr *tmp;
    while((tmp = realloc((*arr), sizeof(lenArr) + (*arr)->len * sizeof(int))) == NULL);
    (*arr) = tmp;
}

void append(lenArr **arr, int elem)
{
    if((*arr)->len <= (*arr)->nxt)
        extend(arr);
    *((*arr)->arr + (*arr)->nxt++) = elem;
}

void flush(lenArr *arr)
{
    int i = 0;
    while(i < arr->nxt)
    {
        printf("%d ", *(arr->arr + i++));
    }

    free(arr);
}

int main(void)
{
    int tmp;
    bool isEven = true;
    even = init();
    while(scanf("%d", &tmp) && tmp != 0)
    {
        isEven ^= true;
        if(isEven)
        {
            append(&even, tmp);
        }
        else
        {
            printf("%d ", tmp);
        }
    }
    flush(even);
}
