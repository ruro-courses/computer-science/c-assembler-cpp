#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

long long trace(int N, int *matrix)
{
    long long ret = 0;
    int i = 0;
    while(i < N)
        ret += *(matrix + i++*(N+1));
    return ret;
}

int main(void)
{
    int *longestMatrix = NULL;
    int *thisMatrix = NULL;
    int *thisElem = NULL;
    int i = 0;
    int num = 0;
    int thisMatrixSize = 0;
    int longestMatrixSize = 0;
    long long thisTrace = 0;
    long long longestTrace = LLONG_MIN;
    scanf("%d", &num);
    scanf("%d", &thisMatrixSize);

    do
    {
        scanf("%*[ \n\t]");
        thisMatrix = realloc(thisMatrix, sizeof(int) * thisMatrixSize * thisMatrixSize);
        thisElem = thisMatrix;
        i = 0;
        while(i < thisMatrixSize * thisMatrixSize)
        {
            scanf("%d", thisElem);
            thisElem += 1;
            i++;
        }
        if((thisTrace = trace(thisMatrixSize, thisMatrix)) > longestTrace)
        {
            free(longestMatrix);
            longestTrace = thisTrace;
            longestMatrix = thisMatrix;
            longestMatrixSize = thisMatrixSize;
            thisMatrix = NULL;
        }
        num--;
    }
    while(num > 0 && scanf("%d", &thisMatrixSize));
    i = 0;
    while(i < longestMatrixSize)
    {
        int j = 0;
        while(j < longestMatrixSize)
            printf("%d ", *(longestMatrix + i*longestMatrixSize + j++));
        printf("%s", "\n");
        i++;
    }
}


