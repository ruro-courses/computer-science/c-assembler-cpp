#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>
#define SIZE 100

typedef struct
{
    int len;
    int nxt;
    int arr[];
} lenArr;

lenArr *init(void)
{
    lenArr *arr;
    while((arr = malloc(sizeof(lenArr) + sizeof(int))) == NULL);
    arr->len = 0;
    arr->nxt = 0;
    return arr;
}

void extend(lenArr **arr)
{
    (*arr)->len *= 2;
    (*arr)->len += 1;
    lenArr *tmp;
    while((tmp = realloc((*arr), sizeof(lenArr) + (*arr)->len * sizeof(int))) == NULL);
    (*arr) = tmp;
}

void append(lenArr **arr, int elem)
{
    if((*arr)->len <= (*arr)->nxt)
        extend(arr);
    *((*arr)->arr + (*arr)->nxt++) = elem;
}

void flush(lenArr *arr)
{
    int i = 0;
    while(i < arr->nxt)
    {
        printf("%d ", *(arr->arr + i++));
    }

    free(arr);
}

__attribute__((pure)) bool isPrime(int a)
{
    if(a == 1)
        return false;
    if(a == 2)
        return true;
    if(!(a&1))
        return false;
    //Divisible by 2
    int divisor = 3;
    int composite = 0;
    while (true)
    {
        composite = a / divisor;
        if (composite < divisor)
        {
            return true;
        }
        //We are searching for a = composite * divisor. Divisor is increasing, Composite decreasing.
        //If they cross, it means we are going over the same values a second time.
        if (a == divisor * composite)
        {
            return false;
        }
        //Division was without remainder
        divisor += 2;
    }
}

int main(void)
{
    int N, tmp, len;
    lenArr *hPrimes = init(), *newhPrimes = init();
    append(&hPrimes, 0);
    scanf("%d", &N);
    if(N == 1){
        printf("%s", "2 3 5 7");
        return 0;
    }
    for(int i = 0; i < N; i++)
    {
        len = hPrimes->nxt;
        for(int j = 0; j < len; j++){
            tmp = hPrimes->arr[j] * 10;
            if(isPrime(tmp + 1))
                append(&newhPrimes, tmp+1);
            if(isPrime(tmp + 2))
                append(&newhPrimes, tmp+2);
            if(isPrime(tmp + 3))
                append(&newhPrimes, tmp+3);
            if(isPrime(tmp + 5))
                append(&newhPrimes, tmp+5);
            if(isPrime(tmp + 7))
                append(&newhPrimes, tmp+7);
            if(isPrime(tmp + 9))
                append(&newhPrimes, tmp+9);
        }
        free(hPrimes);
        hPrimes = newhPrimes;
        newhPrimes = init();
    }
    flush(hPrimes);
}
