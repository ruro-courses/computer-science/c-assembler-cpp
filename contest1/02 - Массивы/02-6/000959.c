#include <stdio.h>

int base[10000];
int tmp[10000];
int transposition[10000];
int len;
int i;

void transpose(void) {
    i = 0;
    while (len - i) {
        tmp[i] = base[transposition[i]];
        i++;
    }
    i = 0;
    while (len - i) {
        base[i] = tmp[i];
        i++;
    }
}

int main(void) {
    scanf("%d", &len);
    i = 0;
    while (len - i) {
        scanf("%d", transposition + i);
        base[i] = i;
        transposition[i++]--;
    }
    transpose();
    transpose();
    transpose();
    i = 0;
    while (len - i) {
        printf("%d ", base[i++] + 1);
    }
}