#include <stdio.h>
#include <stdbool.h>

bool base[10000];
int transposition[10000];
int len;
int i;

int main(void) {
    scanf("%d", &len);
    i = 0;
    while (len - i) {
        scanf("%d", transposition + i);
        if (transposition[i] < 1) {
            printf("No");
            return 0;
        }
        base[--transposition[i++]] = true;
    }
    i = 0;
    while (len - i) {
        if (!base[i++]) {
            printf("No");
            return 0;
        }
    }
    printf("Yes");
}