#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main(void) {
    bool board[10][10] = {[0 ... 9] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
    char inp[129];
    int i, j, x, y, len, count;
    scanf("%128s", inp);
    len = strlen(inp);
    i = 0;
    while (len - i) {
        x = inp[i] - 'a' + 1;
        y = inp[i + 1] - '0';
        board[x - 1][y] = true;
        board[x - 1][y - 1] = true;
        board[x - 1][y + 1] = true;
        board[x][y] = true;
        board[x][y - 1] = true;
        board[x][y + 1] = true;
        board[x + 1][y] = true;
        board[x + 1][y - 1] = true;
        board[x + 1][y + 1] = true;
        i += 2;
    }
    count = 0;
    for (i = 1; i < 9; i++) {
        for (j = 1; j < 9; j++) {
            count += !(board[i][j]);
        }
    }
    printf("%d", count);
}