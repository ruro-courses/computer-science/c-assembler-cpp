#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

short i, j, k, N, out[10000] = {0} ,inp[10000] = {0};
bool isPlaced[10000] = {0};

int main(void) {
    scanf("%hd", &N);
    i = 0;
    while (N - i) {
        scanf("%hd", &inp[i++]);
    }
    i = 0;
    while (N - i){
        if (inp[i] < 0 || inp[i] > N - i - 1){
            printf("-1");
            return 0;
        }
        j = N;
        while(inp[i]>=0){
            inp[i]-=!isPlaced[j--];
        }
        isPlaced[++j] = true;
        out[i] = j;
        i++;
    }
    i = 0;
    while (N - i){
        printf("%hd ", out[i++]);
    }
}
