#include <stdio.h>

int main(void) {
    long int arr[2][10000];
    int smallArrSize, bigArrSize, A, B, i, bigArrIndex;

    scanf("%d", &A);
    i = 0;
    while (A - i) {
        scanf("%ld", arr[0] + i++);
    }
    scanf("%d", &B);
    i = 0;
    while (B - i) {
        scanf("%ld", arr[1] + i++);
    }

    bigArrIndex = (B > A);
    smallArrSize = (bigArrIndex) ? A : B;
    bigArrSize = (bigArrIndex) ? B : A;

    i = 0;
    while (smallArrSize - i) {
        printf("%ld %ld ", arr[0][i], arr[1][i]);
        i++;
    }
    while (bigArrSize - i) {
        printf("%ld ", arr[bigArrIndex][i]);
        i++;
    }
}