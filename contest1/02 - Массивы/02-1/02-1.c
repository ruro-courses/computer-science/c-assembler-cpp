#include <stdio.h>

int main(void) {
    long int arr[10000];
    int i = 0;
    do {
        scanf("%ld", arr + i);
    } while (arr[i++]);
    i--;
    while (i--) {
        printf("%ld ", arr[i]);
    }
}