#include <stdio.h>
#include <stdlib.h>

#define min(a, b) ((a)<(b))?(a):(b)
#define max(a, b) ((a)>(b))?(a):(b)

void die(void) {
    printf("-1");
    exit(0);
}

int main(void) {
    unsigned sum = 0, i, j, x[8], y[8], xCheck, yCheck, board[8][8];
    i = 0;
    while (8 - i) { scanf("%u", x + i++); }
    i = 0;
    while (8 - i) { scanf("%u", y + i++); }
    i = 0;
    while (8 - i) {
        j = 0;
        while (8 - j) {
            sum += board[i][j] = min(x[i], y[j]);
            j++;
        }
        i++;
    }
    i = 0;
    while (8 - i) {
        j = xCheck = yCheck = 0;
        while (8 - j) {
            xCheck = max(xCheck, board[i][j]);
            yCheck = max(yCheck, board[j][i]);
            j++;
        }
        if (xCheck != x[i] || yCheck != y[i]) { die(); }
        i++;
    }
    printf("%u", sum);
}