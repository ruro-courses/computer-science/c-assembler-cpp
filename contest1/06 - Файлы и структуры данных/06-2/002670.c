#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

int main(void)
{
    int N, M, lastPerm, i;
    FILE *input = fopen("input.txt","r"), *output = fopen("output.txt","w");
    fscanf(input, "%d %d", &N, &M);
    while(--M){
        fscanf(input, "%*d");
    }
    fscanf(input, "%d", &lastPerm);
    lastPerm--;
    for(i = 1; i <= N; i++)
    {
        fprintf(output, "%d ", (i+lastPerm-1)%N + 1);
    }
}
