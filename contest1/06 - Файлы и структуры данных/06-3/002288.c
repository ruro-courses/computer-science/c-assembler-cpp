﻿#include <stdio.h>
#include <malloc.h>

#define SIZE 4000

struct list {
    int data;
    struct list *lt;
    struct list *gt;
};

struct list *lsort(struct list *start, int maxLen) {
    struct list *proc = NULL, *element;
    int i = 1;
    while (i <= maxLen) {
        proc = start;
        element = start + i;
        if (element->data < proc->data) {
            while (proc->lt != NULL && element->data < proc->lt->data)
                proc = proc->lt;
            if (proc->lt != NULL) {
                proc->lt->gt = element;
                element->lt = proc->lt;
            }
            proc->lt = element;
            element->gt = proc;
        } else {
            while (proc->gt != NULL && element->data > proc->gt->data)
                proc = proc->gt;
            if (proc->gt != NULL) {
                proc->gt->lt = element;
                element->gt = proc->gt;
            }
            proc->gt = element;
            element->lt = proc;
        }
        i++;
    }
    while (start->lt)
        start = start->lt;
    return start;
}

int main(void) {
    FILE *input, *output;
    input = fopen("input.txt", "r");
    output = fopen("output.txt", "w");
    struct list *l;
    int tmp;
    while (!(l = calloc(SIZE, sizeof(struct list))));
    int i = 0;
    while (fscanf(input, "%d", &tmp) == 1 && i <= SIZE) {
        (l + i++)->data = tmp;
    }
    l = lsort(l, --i);
    while (l) {
        fprintf(output, "%d ", l->data);
        l = l->gt;
    }
    fclose(input);
    fclose(output);
}