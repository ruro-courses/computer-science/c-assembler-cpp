#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <endian.h>


int main(void)
{
    FILE *input = fopen("matrix.in", "r"), *output = fopen("trace.out", "w");

    int16_t size;

    fread(&size, sizeof(int16_t), 1, input);

    size = be16toh(size);

    int32_t matrix[size][size];

    fread(&matrix, sizeof(int32_t), size*size, input);

    int i, j;
    for(i = 0; i < size; i++)
        for(j = 0; j < size; j++)
            matrix[i][j] = be32toh(matrix[i][j]);

    int64_t total = 0;

    for(i = 0; i < size; i++){
        total += matrix[i][i];
    }

    total = htobe64(total);

    fwrite(&total, sizeof(int64_t), 1, output);

    fclose(input);
    fclose(output);
}
