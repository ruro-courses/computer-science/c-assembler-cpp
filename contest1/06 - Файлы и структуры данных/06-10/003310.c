#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>

struct node{
    struct node *next;
    struct node *prev;
};

int main(void)
{
    FILE *input = fopen("input.txt", "r"), *output = fopen("output.txt", "w");
    int N, M;
    fscanf(input, "%d %d ", &N, &M);
    struct node list[N+1]; //list->next is first element
    int i;
    list[0].prev = NULL;
    list[N].next = NULL;
    for(i = 0; i < N; i++){
        list[i].next = list+i+1;
        list[i+1].prev = list+i;
    }
    int begin, end;
    while(M--){
        fscanf(input, "%d %d ", &begin, &end);
        //beforeBegin->afterEnd
        if((list+begin)->prev == list)
            continue;

        (list+begin)->prev->next = (list+end)->next;
        if((list+end)->next)            (list+end)->next->prev = (list+begin)->prev;
        //end->after0
        (list+end)->next = list->next;
        list->next->prev = list+end;
        //0->begin
        list->next = list + begin;
        (list+begin)->prev = list;

    }
    struct node *ptr = list->next;
    while(ptr){
        fprintf(output, "%d ", ptr - list);
        ptr = ptr->next;
    }
    fclose(input);
    fclose(output);
}
