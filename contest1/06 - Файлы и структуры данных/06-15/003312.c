#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>
#include <search.h>

struct key_data
{
    char name[101];
    unsigned int ip;
};

int cmp(const void *a, const void *b)
{
    return strcmp(((struct key_data *)a)->name,((struct key_data *)b)->name);
}

int main(void)
{
    FILE *input = fopen("input.txt", "r"), *output = fopen("output.txt", "w");

    int N, M;
    fscanf(input, "%d ", &N);

    struct key_data memory[N];
    void *root = NULL;

    int i;
    for(i = 0; i < N; i++)
    {
        fscanf(input, "%100s %u", memory[i].name, &(memory[i].ip));
        tsearch(memory+i, &root, cmp);
    }

    fscanf(input, "%d ", &M);

    struct key_data request, **ret;

    while(M--)
    {
        fscanf(input, "%100s", request.name);
        ret = (struct key_data **)tfind(&request, &root, cmp);
        if(ret && *ret)
            fprintf(output, "%u\n", (*ret)->ip);
        else
            fprintf(output, "-1\n");
    }

    fclose(input);
    fclose(output);
}
