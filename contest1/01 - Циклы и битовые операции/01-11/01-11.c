#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    long unsigned int i = 0;
    const long unsigned int mask = 0b11111111000000000000000000000000;

    scanf("%lu", &i);
    printf("%lu", (~i&mask) + (i&~mask));
}
