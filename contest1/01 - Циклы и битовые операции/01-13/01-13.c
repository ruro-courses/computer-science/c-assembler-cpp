#include <stdlib.h>
#include <stdio.h>

double powr(double X, int N){
    return (N)?X*powr(X,N-1):1;
}

long long int factr(int N){
    return (N)?(long long)N*factr(N-1):1;
}

int main(void){
    double X, sum = 0;
    int N = 0;
    scanf("%lf %d", &X, &N);
    while(N)
    {
        sum += (2*(N%2)-1)*powr(X, 2*N-1)/factr(2*N-1);
        N--;
    }
    printf("%.6lf", sum);
}
