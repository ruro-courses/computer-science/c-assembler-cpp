#include <stdio.h>
#include <stdlib.h>

int main(void){
    int X, Y;
    scanf("%d%d", &X, &Y);
    X = (X>0)?X:-X;
    Y = (Y>0)?Y:-Y;
    printf("%d", 2*((X>Y)?X:Y) - (X+Y)%2);
}