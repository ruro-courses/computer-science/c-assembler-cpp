#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int N, max, count, tmp;
    scanf("%d", &N);
    scanf("%d", &max);
    count = 1;
    for (int i = 0; i<N-1 ; i++)
    {
        scanf("%d", &tmp);
        if(tmp == max)
        {count++;}
        else if(tmp>max)
        {max = tmp;
        count = 1;}
    }
    printf("%d", count);
}
