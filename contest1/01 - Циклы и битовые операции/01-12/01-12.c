#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(void)
{
    int N, max, max2, max3, tmp;
    scanf("%d", &N);
    max = max2 = max3 = INT_MIN;
    for (int i = 0; i<N ; i++)
    {
        scanf("%d", &tmp);
        if (tmp>max3)
        {
            max = max2;
            max2 = max3;
            max3 = tmp;
        }
        else if (tmp>max2)
        {
            max = max2;
            max2 = tmp;
        }
        else if (tmp>max)
        {
            max = tmp;
        }
    }
    printf("%d\n%d\n%d", max3, max2, max);
}
