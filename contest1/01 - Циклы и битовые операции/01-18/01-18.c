#include <stdio.h>
#include <stdlib.h>

int main(void) {
    unsigned short a[4], aT[4] = {0, 0, 0, 0};
    scanf("%1hx%1hx%1hx%1hx", a, a + 1, a + 2, a + 3);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            aT[i] <<= 1;
            aT[i] += !!(a[j] & (8 >> i));
        }
    }
    printf("%x%x%x%x", aT[0], aT[1], aT[2], aT[3]);
}