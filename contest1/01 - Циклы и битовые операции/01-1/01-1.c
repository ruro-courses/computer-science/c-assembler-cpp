#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int N, max, min, tmp;
    scanf("%d", &N);
    scanf("%d", &tmp);
    max = min = tmp;
    for (int i = 0; i<N-1 ; i++)
    {
        scanf("%d", &tmp);
        if(tmp<min)
        {min = tmp;}
        else if(tmp>max)
        {max = tmp;}
    }
    printf("%d", max-min);
}
