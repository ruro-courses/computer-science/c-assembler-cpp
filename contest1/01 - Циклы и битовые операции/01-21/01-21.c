#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void) {
    int a[] = {0, 0};
    scanf("%d", a);
    int diff, weight, count = 0, size = (int) ceil(log(a[0]) / log(3));
    while (size >= 0 && a[0] != a[1]) {
        if (a[0] > 1000000 || a[1] > 1000000) {
            printf("-1");
            return 0;
        }
        diff = abs(a[0] - a[1]);
        weight = pow(3, size);
        a[a[0] > a[1]] += (weight <= 2 * diff) ? (count++, weight) : 0;
        size--;
    }
    printf("%d", count);
}