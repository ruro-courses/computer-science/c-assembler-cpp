#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#define Letters "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

typedef enum {PLS = '+', MIN = '-', MUL = '*', DIV = '/', NON = '\0'} operation;

void pprint(unsigned num1, operation op, unsigned num2, unsigned res, bool negative)
{
    if (res <= 120000)
        printf("%u %c %u = %s%u\n", num1, op, num2, (negative) ? "-" : "", res);
}

void parseNumOper(char *pStr)
{
    char *tpStr;
    operation lastOper = NON;
    unsigned numPrev = 0, numThis = 0;

    while(*pStr != '\0')
    {
        while(*pStr == '+' || *pStr == '-' || *pStr == '*' || *pStr == '/')
        {
            pStr++;
            lastOper = NON;
        }
        errno = 0;
        numThis = strtoul(pStr, &tpStr, 10);
        if(!errno && tpStr != pStr)
            switch(lastOper)
            {
            case PLS:
                pprint(numPrev, lastOper, numThis, numPrev + numThis, false);
                break;
            case MIN:
                pprint(numPrev, lastOper, numThis, (numPrev > numThis) ? numPrev - numThis : numThis - numPrev, (numPrev < numThis));
                break;
            case MUL:
                pprint(numPrev, lastOper, numThis, numPrev * numThis, false);
                break;
            case DIV:
                if (numThis != 0)
                    pprint(numPrev, lastOper, numThis, numPrev / numThis, false);
                break;
            case NON:
                break;
            default:
                break;
            }
        pStr = tpStr;
        lastOper = *(pStr++);
        numPrev = numThis;
    }
}

int main(void)
{
    char input[2001];
    scanf("%2001s", input);
    parseNumOper(input);
}
