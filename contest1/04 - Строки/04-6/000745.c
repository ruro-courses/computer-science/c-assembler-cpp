#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int K, len, gaps, i, j, fill, extra;
char inp[1000001];

int main(void) {
    scanf("%d ", &K);
    fgets(inp, 1000000, stdin);
    gaps = 0;
    i = 0;
    while(inp[i] != '\0'){
        if(inp[i] == '\n'){
            inp[i] = '\0';
            break;
        }
        gaps += (inp[i++] == ' ');
    }
    len = strlen(inp);
    fill = (K - len) / gaps;
    extra = (K - len) % gaps;
    i = 0;
    while(inp[i] != '\0'){
        if(inp[i] == ' '){
            j = 0;
            j -= !!extra;
            extra -= !!extra;
            while(fill - j){
                printf("%c", ' ');
                j++;
            }
        }
        printf("%c", inp[i]);
        i++;
    }
}

