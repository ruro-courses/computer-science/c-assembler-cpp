#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define setBit(n) (1<<(n))
#define chkBit(n, a) (!((1<<(n))&(a)))

char towns[10][21];
int length[10], num;

bool chkName(int first, int second){
    int first_end = 0;
    while(towns[first][first_end++] != '\0');
    first_end--;
    return (towns[first][first_end-1] == towns[second][0]);
}

int combine(int N, int usedMask){
    usedMask |= setBit(N);
    int ret = 0;
    int tmp = 0;
    int i = 0;
    while(num - i){
        if(chkBit(i, usedMask) && chkName(N, i)){
            ret = (ret > (tmp = combine(i, usedMask))) ? ret : tmp;
        }
        i++;
    }
    return ret+1;
}

int main(void) {
    scanf("%d", &num);
    int i = 0;
    while(num - i){
        scanf("%20s", towns[i++]);
    }

    i = 0;
    int max = 0;
    while(num - i){
        length[i] = combine(i, 0);
        max = (max > length[i]) ? max : length[i];
        i++;
    }
    i = 0;
    printf("%d\n", max);
    while(num - i){
        if(length[i] == max){
            printf("%s\n", towns[i]);
        }
        i++;
    }
}
