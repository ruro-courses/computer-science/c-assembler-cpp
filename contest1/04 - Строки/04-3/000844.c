#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main(void) {
    int i = 0, j = 0, len;
    char inp[10001];
    bool good = false;
    fgets(inp, 10001, stdin);
    len = strlen(inp);
    while(inp[len-1] == '\n'){
        inp[--len] = '\0';
    }
    while(len-i++){
        j = i;
        good = true;
        while(len-j){
            good &= (inp[j] == inp[j-i]);
            j++;
        }
        if(good){
            printf("%d ", i);
        }
    }
}

