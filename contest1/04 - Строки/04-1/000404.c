#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


int main(void) {
    char a[10001], b[10001], tmp[10001];
    char *pA = a, *pB = b, *pT = tmp;
    int i, j, lenA, lenB;
    bool flip = false;
    scanf("%10000s %10000s", pA, pB);
    lenA = strlen(pA);
    lenB = strlen(pB);

    if(lenA<lenB){
        strcpy(tmp,b);
        strcpy(b,a);
        strcpy(a,tmp);
        flip = true;
        i = lenA;
        lenA = lenB;
        lenB = i;
    }

    i = lenB;
    strcpy(pT, pA);
    pT[lenB] = '\0';

    while(i){
    if(!strcmp(pT, pB)){break;}
    pT[i-1] = '\0';
    pB += 1;
    i--;
    }

    pA = a;
    pB = b;
    pT = tmp;

    j = lenB;
    strcpy(pT, pB);
    pA += lenA-lenB;

    while(j){
    if(!strcmp(pT, pA)){break;}
    pT[j-1] = '\0';
    pA += 1;
    j--;
    }

    printf("%d %d", flip?j:i, flip?i:j);
}
