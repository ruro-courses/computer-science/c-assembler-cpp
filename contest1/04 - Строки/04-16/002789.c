#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define ZERONUM 2000
#define ELIMINATED -1

int inpLen;
int winLen;
int win[ZERONUM];
int winner;
char inp[2000001];

char inpEl(int i)
{
    return (i >= 0) ? inp[i % inpLen] : inp[inpLen + i % inpLen];
}

int* winEl(int i)
{
    return (i >= 0) ? win + i % winLen : win + inpLen + i % inpLen;
}

bool compete(int i)
{
    int j = -1;
    int lastCandidate = 0;
    bool isZeroAvailable = false;
    while(*winEl(++j) == ELIMINATED);           //Scroll until first non-eliminated start
    if(i >= inpLen)                             //Wrapping around means all 'win' candidates are equal
    {
        winner = *winEl(j);
        return false;
    }
    while(j < winLen)                           //For all 'win' candidates
    {
        if(inpEl(*winEl(j)+i) == '0')           //If zero is found, break and eliminate all non-zero candidates
        {
            isZeroAvailable = true;
        }
        while(*winEl(++j) == ELIMINATED);       //Scroll until the next candidate
    }
    lastCandidate = *winEl(j);
    while(*winEl(++j) == ELIMINATED);
    if(lastCandidate == *winEl(j))              //If only one candidate is Left, he is the winner
    {
        winner = *winEl(j);
        return false;
    }
    if(!isZeroAvailable)                        //If all candidates are '1' exit and check next char
        return true;
    j = -1;
    while(j < winLen)                           //For all 'win' candidates
    {
        while(*winEl(++j) == ELIMINATED);       //Scroll until the next candidate

        if(inpEl(*winEl(j)+i) == '1')
        {
            *winEl(j) = ELIMINATED;
        }
    }
    return true;
}

int main(void)
{
    scanf("%2000001s", inp);
    inpLen = strlen(inp);

    int i = 0;
    winLen = -1;
    while(inpLen > i)
    {
        if(inpEl(i) == '0' && inpEl(i-1) != '0')
            win[++winLen] = i;
        i++;
    }

    winLen++;

    if(winLen == 0)
    {
        printf("%s", inp);
        return 0;
    }

    i = 1;
    while(compete(i++));

    i = 0;
    while(inpLen - i)
    {
        putchar(inpEl(winner+i));
        i++;
    }

    return 0;
}


