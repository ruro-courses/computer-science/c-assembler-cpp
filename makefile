vpath %.c ejudge1
vpath %.cpp ejudge2

SOURCES_C = $(wildcard ejudge1/r_*.c)
SOURCES_CPP = $(wildcard ejudge2/*.cpp)
EXECUATBLES = $(notdir $(SOURCES_C:.c=)) $(notdir $(SOURCES_CPP:.cpp=))

COMFLAGS = -O3 -lm -Wall -Werror -Wignored-qualifiers -Wformat-security -Wswitch-default -Wfloat-equal -Wshadow -Wpointer-arith -Wtype-limits -Wempty-body -Wlogical-op -Wmissing-field-initializers

CXXFLAGS = $(COMFLAGS) -std=c++17 -Winit-self -Wctor-dtor-privacy  -Wnon-virtual-dtor -Wstrict-null-sentinel  -Wold-style-cast -Woverloaded-virtual -Wsign-promo -Weffc++ -pthread -lprofiler
CXX = g++

CFLAGS = $(COMFLAGS) -std=gnu11 -Wno-pointer-sign -Wformat -Wformat-overflow -Wnull-dereference -Wshift-negative-value -Wduplicated-branches -Wpointer-compare -Wwrite-strings -Wdangling-else -Wstrict-prototypes -Wold-style-declaration -Wold-style-definition -Wmissing-parameter-type -Wnested-externs -Wvla-larger-than=4096
CC = gcc

.PHONY:all
all: $(EXECUATBLES)

.PHONY:clean
clean:
	rm -f $(EXECUATBLES)
