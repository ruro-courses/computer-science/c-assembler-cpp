%include "io.inc"

SECTION .bss
strarr resb 5511

SECTION .data
sregexp_clr db "%*u ",0
sregexp db "%10s ",0
pregexp db "%u ",0

SECTION .text
GLOBAL CMAIN
EXTERN printf
EXTERN strcmp
EXTERN scanf
CMAIN:
    mov ebp, esp
    sub esp,0x00000008
    and esp,0xfffffff0
    
    mov dword[esp],sregexp_clr
    call scanf            ;clear the number of strings
    
    xor edi,edi           ;number of unique strings
    xor ebx,ebx           ;number of string we are comparing to
    
.read:
    lea esi,[edi+edi*8]
    lea esi,[esi+edi*2]
    lea esi,[strarr+esi]
    mov dword[esp+4],esi    ;strarr+11*ecx
    mov dword[esp],sregexp
    call scanf
    cmp eax,-1
    je .stop
    xor ebx,ebx
.check:
    cmp edi,ebx
    je .unique
    lea eax,[ebx+ebx*8]
    lea eax,[eax+ebx*2]
    lea eax,[strarr+eax]
    mov dword[esp+4],eax
    mov dword[esp],esi
    call strcmp
    test eax,eax
    jz .read
    inc ebx
    jmp .check
.unique:
    inc edi
    jmp .read
.stop:
    mov dword[esp+4],edi
    mov dword[esp],pregexp
    call printf
    mov esp,ebp
    xor eax,eax
    ret