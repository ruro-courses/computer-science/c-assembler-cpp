%include "io.inc"

SECTION .bss
str1 resb 1001
str2 resb 1001

SECTION .data
sregexp db "%s %s ",0
ans db "1 2",0,"2 1",0,"0  ",0

SECTION .text
GLOBAL CMAIN
EXTERN printf
EXTERN strstr
EXTERN scanf
CMAIN:
    xor ebx,ebx
    mov ebp,esp
    sub esp,0x0000000C
    and esp,0xfffffff0
rpt_main:
    mov dword[esp+8],str1
    mov dword[esp+4],str2
    mov dword[esp],sregexp
    call scanf

    mov dword[esp+4],str2
    mov dword[esp],str1
    call strstr
    test eax,eax
    jnz res_main
    add ebx,4
    

    mov dword[esp+4],str1
    mov dword[esp],str2
    call strstr
    test eax,eax
    jnz res_main
    add ebx,4
    
res_main:
    lea eax,[ans+ebx]
    mov dword[esp],eax
    call printf
    mov esp,ebp
    xor eax,eax
    ret