%include "io.inc"

SECTION .bss
number resd 1

SECTION .data
sregexp db "%u ",0
pregexp db "0x%08X",10,0

SECTION .text
GLOBAL CMAIN
EXTERN printf
EXTERN scanf
CMAIN:
    mov ebp,esp
    sub esp,0x00000008
    and esp,0xfffffff0
    mov eax,1
rpt_main:
    mov dword[esp+4],number
    mov dword[esp],sregexp
    call scanf
    cmp eax,-1
    je stp_main
    mov eax,dword[number]
    mov dword[esp+4],eax
    mov dword[esp],pregexp
    call printf
    jmp rpt_main
stp_main:
    mov esp,ebp
    xor eax,eax
    ret