%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_CHAR    eax      ;eax - x from
    GET_DEC     4,ebx    ;ebx - y from
    GET_CHAR    ecx      ;clear the space
    GET_CHAR    ecx      ;ecx - x to
    GET_DEC     4,edx    ;edx - y to
    
    sub         eax,ecx
    sub         ebx,edx
    
    mov         esi,eax
    sar         esi,31
    shl         esi,1
    inc         esi
    imul        eax,esi

    mov         esi,ebx
    sar         esi,31
    shl         esi,1
    inc         esi
    imul        ebx,esi
    
    add         eax,ebx
    PRINT_DEC   4,eax
    
    xor         eax, eax
    ret