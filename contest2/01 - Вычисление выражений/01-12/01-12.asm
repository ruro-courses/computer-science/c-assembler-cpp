%include "io.inc"

section .data
arr db "2S3S4S5S6S7S8S9STSJSQSKSAS2C3C4C5C6C7C8C9CTCJCQCKCAC2D3D4D5D6D7D8D9DTDJDQDKDAD2H3H4H5H6H7H8H9HTHJHQHKHAH"

section .text
global CMAIN
CMAIN:
    GET_DEC    4,eax
    dec        eax
    imul       eax,2
    PRINT_CHAR arr+eax
    PRINT_CHAR arr+eax+1
    xor        eax,eax
    ret