%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_DEC 4,eax
    GET_DEC 4,ebx
    
    mov ecx,eax
    sub ecx,1
    shr eax,1
    shr ecx,1
    imul eax,41
    imul ecx,42
    add eax,ecx
    add eax,ebx

    PRINT_DEC 4,eax
    xor eax, eax
    ret