%include "io.inc"

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    GET_CHAR eax
    GET_DEC 4,ebx
    sub eax,"A"-1
    neg eax
    neg ebx
    add eax, 8
    add ebx, 8
    mul ebx
    
    shr eax,1
            
    PRINT_DEC 4,eax
    xor eax, eax
    ret