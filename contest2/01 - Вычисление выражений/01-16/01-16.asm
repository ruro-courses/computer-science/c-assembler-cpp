%include "io.inc"

section .bss
    a1 resw 2
    a2 resw 2
    b1 resw 2
    b2 resw 2
    c  resw 2
    d  resw 2

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    GET_UDEC     4,a1
    GET_UDEC     4,a2
    GET_UDEC     4,b1
    GET_UDEC     4,b2
    GET_UDEC     4,c
    GET_UDEC     4,d
    
    ;x = a1*~a2*c
    mov eax,[a2]
    not eax
    and eax,[a1]
    and eax,[c]
    
    ;x += b1*~b2*d
    mov ecx,[b2]
    not ecx
    and ecx,[b1]
    and ecx,[d]
    or eax, ecx
    
    ;x += a2*b2(a1^b1)*(c^d)
    mov ecx,[a2]
    and ecx,[b2]
    mov edx,[a1]
    xor edx,[b1]
    and ecx,edx
    mov edx,[c]
    xor edx,[d]
    and ecx,edx
    or eax,ecx
    
    ;y = ~a1*a2*c
    mov ebx,[a1]
    not ebx
    and ebx,[a2]
    and ebx,[c]
    
    ;y += ~b1*b2*d
    mov ecx,[b1]
    not ecx
    and ecx,[b2]
    and ecx,[d]
    or ebx, ecx
    
    ;y += a1*a2*(x+c)
    mov ecx,eax
    xor ecx,[c]
    and ecx,[a1]
    and ecx,[a2]
    or ebx,ecx
    
    ;y += b1*b2*(x+d)
    mov ecx,eax
    xor ecx,[d]
    and ecx,[b1]
    and ecx,[b2]
    or ebx,ecx
    
    PRINT_UDEC   4,eax
    PRINT_CHAR   " "
    PRINT_UDEC   4,ebx
    
    xor         eax, eax
    ret