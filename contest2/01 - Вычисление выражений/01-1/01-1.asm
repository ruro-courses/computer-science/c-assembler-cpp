%include "io.inc"

section .text
global CMAIN
CMAIN:
    GET_DEC 4,eax
    GET_DEC 4,ebx
    GET_DEC 4,ecx
    GET_DEC 4,edx
    GET_DEC 4,esi
    imul eax,esi
    imul ebx,esi
    imul esi,esi
    imul ecx,esi
    imul edx,esi
    add eax,ecx
    add ebx,edx
    PRINT_DEC 4,eax
    PRINT_CHAR " "
    PRINT_DEC 4,ebx
    xor eax, eax
    ret