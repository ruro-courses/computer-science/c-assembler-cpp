%include "io.inc"

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    GET_DEC 4,eax
    GET_DEC 4,ebx
    GET_DEC 4,ecx
    GET_DEC 4,esi
    
    xor edi,edi
    mul ebx
    mov edi,edx
    mul ecx
    imul edi,ecx
    add edx,edi
    
    div esi
    
    PRINT_DEC 4,eax
    xor eax, eax
    ret