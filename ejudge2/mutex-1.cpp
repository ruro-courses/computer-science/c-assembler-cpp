#include <iostream>
#include <thread>
#include <vector>
#include <mutex>


const size_t TNUM = 3, RNUM = 1000000;
double arr[TNUM];


int main()
{
    std::vector<std::thread> threads;
    std::mutex control;

    // Start TNUM threads.
    for (size_t id = 0; id < TNUM; id++) {
        threads.emplace_back(
                // Start new thread.
                [id, &threads, &control]()
                {
                    for (size_t j = 0; j < RNUM; j++) {
                        std::scoped_lock<std::mutex> lock(control);

                        arr[id] += 100 * (id + 1);
                        arr[(id + 1) % TNUM] -= 1 + 100 * (id + 1);

                    }
                }
        );
    }

    // Wait for all threads.
    for (std::thread &t : threads) {
        t.join();
    }

    // Print out results.
    for (double value : arr) {
        std::printf("%.10g\n", value);
    }
}