
#include <iostream>
#include <iterator>


std::string to_infix(std::string &NPN)
{
    char current = NPN.back();
    NPN.pop_back();

    if (!std::isalpha(current)) {
        std::string second = to_infix(NPN);
        std::string first = to_infix(NPN);
        return "(" + first + current + second + ")";
    } else {
        return std::string(1, current);
    }
}

int main()
{
    std::istream_iterator<char> input_start(std::cin);
    std::istream_iterator<char> input_end;
    std::string RPN(input_start, input_end);
    std::cout << to_infix(RPN) << std::endl;
}