#include <functional>


template<class It, class Cmp = std::less<>>
void selection_sort(It first, It last, Cmp comparator = std::less<>())
{
    while (first != last) {
        It runner = first, least = first;
        runner++;
        while (runner != last) {
            least = comparator(*runner, *least) ? runner : least;
            runner++;
        }

        std::swap(*least, *first);
        first++;
    }
};