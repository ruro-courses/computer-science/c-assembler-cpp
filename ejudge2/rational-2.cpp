#include <sstream>
#include <cstdlib>


int gcd(int numerator, int denominator)
{
    return denominator == 0 ? std::abs(numerator) :
            gcd(denominator, std::abs(numerator) % denominator);
}

int simplify(int &numerator, int &denominator)
{
    if (denominator < 0) {
        numerator *= -1;
        denominator *= -1;
    }

    int factor = gcd(numerator, denominator);
    numerator /= factor;
    denominator /= factor;

    return factor;
}


class Rational
{
private:
    int a, b;

public:
    Rational(const Rational &source) : a(source.a), b(source.b)
    {
        simplify(a, b);
    }

    Rational(int numerator = 0, int denominator = 1) : a(numerator), b(denominator)
    {
        simplify(a, b);
    }

    Rational &Add(const Rational &other)
    {
        int oa = other.a;
        int ob = other.b;

        int factor = simplify(b, ob);
        a *= ob;
        oa *= b;

        a += oa;
        b *= ob * factor;

        simplify(a, b);
        return *this;
    }

    Rational &Substract(const Rational &other)
    {
        return Add(Rational(-other.a, other.b));
    }

    Rational &Multiply(const Rational &other)
    {
        int oa = other.a;
        int ob = other.b;

        simplify(oa, b);
        simplify(a, ob);

        a *= oa;
        b *= ob;

        return *this;
    }

    Rational &Divide(const Rational &other)
    {
        return Multiply(Rational(other.b, other.a));
    }

    int CompareTo(const Rational &other) const
    {
        Rational &valDiff = Rational(*this).Substract(other);
        return (valDiff.a > 0) - (valDiff.a < 0);
    }

    bool EqualTo(const Rational &other) const
    {
        return !CompareTo(other);
    }

    bool IsInteger() const
    {
        return (b == 1);
    }

    std::string ToString() const
    {
        std::stringstream ss;
        ss << a << ':' << b;
        return ss.str();
    }
};


#include <iostream>

int main()
{
    Rational r = Rational(0,5);
    std::cout << r.ToString() << std::endl;
}
