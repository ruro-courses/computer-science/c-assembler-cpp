
template<typename T, typename P>
int myfilter(T begin, T end, P predicate, typename T::value_type value = typename T::value_type())
{
    int count = 0;
    while (begin != end) {
        if (predicate(*begin)) {
            *begin = value;
            count++;
        }
        begin++;
    }
    return count;
}