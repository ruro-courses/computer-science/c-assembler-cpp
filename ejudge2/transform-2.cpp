#include <iostream>

// L1 = 1^n 0^m 1^m 0^n
// L2 = 1^m 0^n+m \n

// S -> 1 R 0 <out0> <out\n>
// R -> 1 R 0 <out0> | 0 <out1> Q 1 <out0>
// Q -> 0 <out1> Q 1 <out0> | eps

char tar;

void get_char()
{
    if (!(std::cin >> tar)) {
        throw std::exception();
    }
}

void S();
void R();
void Q();

void S()
{
    get_char();
    // tar == '1' for every valid sequence, no need to check
    R();
    get_char();
    // tar == '0' for every valid sequence, no need to check
    std::cout << '0' << std::endl;
}

void R()
{
    get_char();
    switch (tar) {
        case '1':
            R();
            get_char();
            // tar == '0' for every valid sequence, no need to check
            break;
        case '0':
            std::cout << '1';
            Q();
            get_char();
            // tar == '1' for every valid sequence, no need to check
            break;
        default:
            // tar is either '0' or '1' for every vaild sequence, no need to check
            break;
    }
    std::cout << '0';
}

void Q()
{
    get_char();
    switch (tar) {
        case '1':
            //eps
            std::cin.putback(tar);
            return;
        case '0':
            std::cout << '1';
            Q();
            get_char();
            // tar == '1' for every valid sequence, no need to check
            break;
        default:
            // tar is either '0' or '1' for every valid sequence, no need to check
            break;
    }
    std::cout << '0';
}


int main()
{
    while (true) {
        try {
            S();
        } catch (...) {
            return 0;
        }
    }
}
