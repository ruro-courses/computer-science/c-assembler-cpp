#include <iostream>

// S -> a^n 0^k b^n 1^k ; n,k > 0

int get_char(char &tar)
{
    // 0  - input end
    // 1  - input ok
    // -1 - input was a word split
    if (!(std::cin >> std::noskipws >> tar)) {
        return 0;
    }
    return std::isspace(tar) ? -1 : 1;
}

char current = '\0';
int n = 0;
int k = 0;
bool error = false;
enum State
{
    A, Zero, B, One, WS
};
State state = WS;
int ctrl = 0;


void word_end()
{
    if (state != WS) {
        if (n || k || state != One) {
            error = true;
        }

        std::cout << !error << std::endl;

        state = WS;
        n = 0;
        k = 0;
        error = false;
    }
}

void countUP(State cur, State prev, int &counter)
{
    if (state == cur || state == prev) {
        state = cur;
        counter++;
    } else {
        error = true;
    }
}

void countDOWN(State cur, State prev, int &counter)
{
    if (counter > 0 && (state == cur || state == prev)) {
        state = cur;
        counter--;
    } else {
        error = true;
    }
}

int main()
{
    while ((ctrl = get_char(current))) {
        if (ctrl == -1) {
            word_end();
            continue;
        }

        if (error) {
            continue;
        }

        if (state == WS)
        {
            state = A;
        }

        switch (current) {
            case 'a':
                countUP(A, WS, n);
                break;
            case '0':
                countUP(Zero, A, k);
                break;
            case 'b':
                countDOWN(B, Zero, n);
                break;
            case '1':
                countDOWN(One, B, k);
                break;
            default:
                error = true;
        }
    }

    word_end();
}