#include <iostream>


int get_char(char &tar)
{
    // 0  - input end
    // 1  - input ok
    // -1 - input was after word split
    int count = 0;
    do {
        if (!(std::cin >> std::noskipws >> tar)) {
            return 0;
        }
        count++;
    } while (std::isspace(tar));
    return count != 1 ? -1 : 1;
}


int main()
{
    char current;
    bool once = true;
    bool error = false;
    int status;
    size_t z = 0, o = 0, zc = 0, oc = 0;
    enum
    {
        zeros, ones, rest
    } state = zeros;

    while ((status = get_char(current))) {
        once = false;

        // While there is something in the input.
        if (status == -1 || error) {
            // If whitespace or error.
            while (status != -1 && status != 0) {
                // Skip to whitespace. (on early error)
                status = get_char(current);
            }

            if (!status) {
                // Input end after whitespace.
                break;
            }
            if (zc != z || oc != o || !z || !o) {
                // If word ended unexpectedly - error.
                error = true;
            }

            // Print and reset.
            std::cout << !error << std::endl;
            state = zeros;
            error = false;
            z = 0;
            o = 0;
            zc = 0;
            oc = 0;
        }

        switch (state) {
            case zeros:
                // Count leading zeros
                if (current == '0') {
                    z++;
                    zc++;
                    break;
                }
                if (!z) {
                    error = true;
                }

                // Fallthrough
            case ones:
                // Count leading ones
                state = ones;
                if (current == '1') {
                    o++;
                    oc++;
                    break;
                }
                if (!o) {
                    error = true;
                }

                // Fallthrough
            case rest:
                state = rest;
                if (zc == z && oc == o) {
                    // Last 0*1* pattern full.
                    zc = 0;
                    oc = 0;
                }
                if (zc < z && current == '0') {
                    // 0* pattern.
                    zc++;
                    break;
                }
                if (zc == z && oc < o && current == '1') {
                    // 1* pattern (only after 0*).
                    oc++;
                    break;
                }

                // Fallthrough
            default:
                // Something else, error.
                error = true;
        }
    }

    if (once) {
        return 0;
    }
    if (zc != z || oc != o || !z || !o) {
        // If word ended unexpectedly - error.
        error = true;
    }
    std::cout << !error << std::endl;
}