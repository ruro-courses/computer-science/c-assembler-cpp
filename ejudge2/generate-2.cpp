#include <iostream>


void print_seq(std::string &buf, size_t pos, size_t len, bool beta)
{
    if (pos == len)
    {
        std::cout << buf << std::endl;
    }
    else {
        buf[pos] = '1';
        print_seq(buf, pos + 1, len, true);
        buf[pos] = '2';
        print_seq(buf, pos + 1, len, true);
        if (!beta) {
            buf[pos] = '3';
            print_seq(buf, pos + 1, len, false);
            buf[pos] = '4';
            print_seq(buf, pos + 1, len, false);
        }
    }
}

int main()
{
    size_t K;
    std::cin >> K;
    std::string buf = std::string();
    buf.resize(K);
    print_seq(buf, 0, K, false);
}