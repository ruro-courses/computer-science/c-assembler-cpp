#include <string>
#include <list>
#include <utility>
#include <algorithm>
#include <map>
#include <iostream>


int pcent_division(int numerator, int denominator)
{
    return static_cast<int>((100 * static_cast<long long>(numerator) + denominator / 2) / denominator);
}


class Election
{
public:
    class KV : public std::pair<std::string, int>
    {
    public:
        KV(const std::string &name, int count = 0)
                : std::pair<std::string, int>(name, count)
        {
        }

        std::pair<std::string, int> get_pcent(int total_voters)
        {
            return std::pair<std::string, int>
                    (first, pcent_division(second, total_voters));
        }

        bool operator<(const KV &other) const
        {
            return second == other.second ? first < other.first : second < other.second;
        }
    };


private:
    const std::list<std::string> candidate_names;
    std::map<std::string, KV> votes;
    int voters;
    int total_votes;
    int invalid_votes;

public:
    Election(const std::list<std::string> &candidates, int voter_num)
            :
            candidate_names(candidates), votes(), voters(voter_num),
            total_votes(0), invalid_votes(0)
    {
        for (std::list<std::string>::const_iterator iterator = candidate_names.begin();
                iterator != candidate_names.end(); ++iterator) {
            votes[*iterator] = KV(*iterator);
        }
    }

    Election &operator+=(const std::string &name)
    {
        for (std::list<std::string>::const_iterator iterator = candidate_names.begin();
                iterator != candidate_names.end(); ++iterator) {
            if (*iterator == name) {
                total_votes++;
                votes[name].second++;
                return *this;
            }

        }
        total_votes++;
        invalid_votes++;
        return *this;
    }

    int get_total_votes()
    {
        return pcent_division(total_votes, voters);
    }

    int get_invalid_votes()
    {
        return pcent_division(invalid_votes, voters);
    }

    std::list<std::pair<std::string, int>> get_results()
    {
        std::list<std::pair<std::string, int> > ret;
        for (std::list<std::string>::const_iterator iterator = candidate_names.begin();
                iterator != candidate_names.end(); ++iterator) {
            ret.push_back(votes[*iterator].get_pcent(voters));
        }
        std::sort(ret.begin(), ret.end());
        return ret;
    }
};


int main()
{
    std::list<std::string> candidates;
    candidates.push_back("napoleon");
    candidates.push_back("loshad");
    candidates.push_back("balyk");
    candidates.push_back("zhir");
    candidates.push_back("grusha");

    int count = 6;
    //std::cin >> count;

    Election election = Election(candidates, count);
    election += "napoleon";

    std::list<std::pair<std::string, int> > res = election.get_results();
    std::cout << election.get_total_votes() << std::endl;
    std::cout << election.get_invalid_votes() << std::endl;
    for (std::list<std::pair<std::string, int> >::iterator it = res.begin();
            it != res.end(); ++it) {
        std::cout << it->first << it->second << std::endl;

    }
}