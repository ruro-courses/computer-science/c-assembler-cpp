#include <stdexcept>
#include <sstream>
#include <iostream>


template<class T>
class Range
{
private:
    T low;
    T high;
public:
    void validate()
    {
        if (low > high) {
            throw std::invalid_argument("`low` value can't be greater then `high`.");
        }
    }

    Range(T l = T(), T h = T())
            : low(l), high(h)
    {
        validate();
    }

    explicit Range(const std::string &s)
            : low(), high()
    {
        std::stringstream stream(s);
        char paren, comma;
        if (stream >> paren &&
                stream >> low &&
                stream >> comma &&
                stream >> high &&
                stream >> paren) {
            validate();
        } else {
            throw std::invalid_argument("Unable to construct range object from string.");
        }
    }

    std::string to_string() const
    {
        std::stringstream stream;
        stream << "(" << low << "," << high << ")";
        return stream.str();
    }

    bool contains_zero() const
    {
        return (low < 0) && (high > 0);
    }

    T get_low() const
    {
        return low;
    }

    T get_high() const
    {
        return high;
    }

    Range operator+(const Range &other) const
    {
        return Range(low + other.low, high + other.high);
    }

    Range operator-(const Range &other) const
    {
        return *this + (-other);
    }

    Range operator*(const Range &other) const
    {
        using std::min;
        using std::max;
        T ll, lh, hl, hh;
        ll = low * other.low;
        lh = low * other.high;
        hl = high * other.low;
        hh = high * other.high;
        T l = min(
                min(ll, lh),
                min(hl, hh)
        );
        T h = max(
                max(ll, lh),
                max(hl, hh)
        );
        if (contains_zero() || other.contains_zero()) {
            return Range(
                    min(
                            l,
                            0
                    ),
                    max(
                            ,
                            0
                    )
            );
        } else {
            return Range(
                    min(min(ll, lh), min(hl, hh)), max(max(ll, lh), max(hl, hh)));
        }
    }

    Range operator-() const
    {
        return Range(-high, -low);
    }
};