#include <cstdint>
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <deque>


int main()
{
    std::uint32_t acc_count, thr_count, iter_count;
    size_t first_id, second_id;
    double first_val, second_val;
    std::vector<std::thread> threads;
    std::vector<double> accounts;
    std::deque<std::mutex> control;

    // Reserve threads, create mutexes and accounts.
    std::cin >> acc_count >> thr_count;
    threads.reserve(thr_count);
    accounts.resize(acc_count);
    control.resize(acc_count);

    // For every thread.
    while (std::cin >> iter_count >> first_id >> first_val >> second_id >> second_val) {
        // Create and start the thread.
        threads.emplace_back(
                [&control, &accounts, iter_count, first_id, second_id, first_val, second_val]()
                {
                    // Bind first and second account and mutex by reference.
                    size_t master_id = first_id;
                    size_t slave_id = second_id;

                    if (master_id > slave_id) {
                        std::swap(master_id, slave_id);
                    }

                    std::mutex &master_mutex = control[master_id];
                    std::mutex &slave_mutex = control[slave_id];
                    double &first_acc = accounts[first_id];
                    double &second_acc = accounts[second_id];

                    // Repeat iter_count times
                    std::uint32_t repeat = iter_count;
                    while (repeat--) {
                        // Lock both mutexes. Using Edsger Dijkstras' algorithm.
                        std::scoped_lock<std::mutex> master_lock(master_mutex);
                        std::scoped_lock<std::mutex> slave_lock(slave_mutex);

                        // Do the accounting.
                        first_acc += first_val;
                        second_acc += second_val;
                    }
                }
        );
    }

    // Wait for threads to finish.
    for (std::thread &t : threads) {
        t.join();
    }

    // Print out results.
    for (double value : accounts) {
        std::printf("%.10g\n", value);
    }
}