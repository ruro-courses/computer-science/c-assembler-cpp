#include <iostream>


int main()
{
    std::string current;
    while (std::cin >> current) {
        bool error = false;
        bool beta = false;
        for (char &c : current) {
            if (!beta && (c == '3' || c == '4')) {
                continue;
            } else if (c == '1' || c == '2') {
                beta = true;
            } else {
                error = true;
                break;
            }
        }
        std::cout << !error << std::endl;
    }
}