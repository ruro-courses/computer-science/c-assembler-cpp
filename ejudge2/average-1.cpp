#include <iostream>
#include <iomanip>
#include <cmath>


using namespace std;

int main()
{
    double next;
    double average = 0;
    double sqAverage = 0;
    int count = 0;
    while (cin >> next) {
        count++;
        average += next;
        sqAverage += next * next;
    }
    average /= count;
    sqAverage /= count;
    double deviation = sqrt(sqAverage - average * average);
    cout << setprecision(10) << average << endl << deviation << endl;
    return 0;
}
