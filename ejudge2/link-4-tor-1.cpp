#include <iostream>


int diff(int a, int b, int mod)
{
    int dist = std::abs(a - b);
    return std::min(dist, std::abs(dist - mod));
}


int main()
{
    int m, n;
    std::cin >> m;
    std::cin >> n;
    int r1, c1, r2, c2;
    while (std::cin >> r1 &&
            std::cin >> c1 &&
            std::cin >> r2 &&
            std::cin >> c2) {
        std::cout << (diff(r1, r2, m) + diff(c1, c2, n)) << std::endl;
    }
}