#include <algorithm>


template<typename T>
struct Coord
{
    typedef T value_type;
    T row;
    T col;

    Coord(T r = T(), T c = T())
            : row(r), col(c)
    {
    }

    T len()
    {
        using std::max;
        return max(row, col);
    }

    Coord<T> operator-(const Coord<T> &other) const
    {
        using std::abs;
        return Coord(
                abs(row - other.row),
                abs(col - other.col));
    }

    Coord<T> reduce(const Coord<T> &bounds) const
    {
        using std::abs;
        using std::min;
        return Coord(
                min(abs(row - bounds.row), row),
                min(abs(col - bounds.col), col));
    }
};


template<typename T>
T dist(Coord<T> bounds, Coord<T> pointA, Coord<T> pointB)
{
    return (pointA - pointB).reduce(bounds).len();
}