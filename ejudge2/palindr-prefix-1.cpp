#include <iostream>
#include <algorithm>


int main()
{
    std::string buf;
    while (std::cin >> buf) {
        size_t i = buf.length();
        while (i > 0) {
            std::string part = buf.substr(0, i);
            std::string rev = part;
            std::reverse(rev.begin(), rev.end());

            if (part == rev) {
                break;
            }
            i--;
        }
        std::cout << buf.substr(0, i) << std::endl;
    }
}