#include <cstdio>


namespace numbers
{
    void copyData(complex *dest, const complex *src, size_t length)
    {
        for (size_t i = 0; i < length; i++) {
            dest[i] = src[i];
        }
    }


    class complex_stack
    {
    private:
        complex *memory;
        size_t max_memory;
        size_t elem_count;

        complex pop()
        {
            elem_count--;
            return memory[elem_count];
        }

        void push(const complex &element)
        {
            if (elem_count + 1 > max_memory) {
                max_memory = 2 * max_memory + 1;
                *this = complex_stack(*this);
            }

            memory[elem_count] = element;
            elem_count++;
        }

    public:
        complex_stack(size_t starting_size = 0) :
                memory(new complex[starting_size]),
                max_memory(starting_size),
                elem_count(0)
        {
        }

        complex_stack(const complex_stack &other) :
                memory(new complex[other.max_memory]),
                max_memory(other.max_memory),
                elem_count(other.elem_count)
        {
            copyData(memory, other.memory, elem_count);
        }

        complex_stack &operator=(const complex_stack &other)
        {
            if (this == &other) {
                return *this;
            }

            max_memory = other.max_memory;
            elem_count = other.elem_count;

            delete[] memory;
            memory = new complex[max_memory];
            copyData(memory, other.memory, elem_count);

            return *this;
        }

        ~complex_stack()
        {
            delete[] memory;
        }

        complex_stack operator<<(const complex &elem) const
        {
            complex_stack copy = *this;
            copy.push(elem);
            return copy;
        }

        complex_stack operator~() const
        {
            complex_stack copy = *this;
            copy.pop();
            return copy;
        }

        complex operator[](size_t index) const
        {
            return memory[index];
        }

        complex operator+() const
        {
            return (*this)[elem_count - 1];
        }

        size_t size() const
        {
            return elem_count;
        }
    };
}