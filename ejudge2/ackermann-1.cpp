#include <iostream>


struct Result
{
    int result;

    Result(int res)
            : result(res)
    {
    }
};

Result Ackermann(int m, int n);

void recurseAckermann(int m, int n)
{
    if (!m) {
        throw Result(n + 1);
    } else if (!n) {
        recurseAckermann(m - 1, 1);
    } else {
        recurseAckermann(m - 1, Ackermann(m, n - 1).result);
    }
}

Result Ackermann(int m, int n)
{
    try {
        recurseAckermann(m, n);
    } catch (Result &r) {
        return r;
    }
    throw std::exception();
}

int main()
{
    int m = 0, n = 0;
    std::cin >> m;
    std::cin >> n;
    std::cout << Ackermann(m, n).result;
}