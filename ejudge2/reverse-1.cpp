
template<typename T>
void myreverse(T begin, T end)
{
    typename T::value_type buf;
    if (begin != end) {
        end--;
    }
    while (begin != end) {
        buf = *begin;
        *begin = *end;
        *end = buf;

        begin++;
        if (begin != end) {
            end--;
        }
    }
}