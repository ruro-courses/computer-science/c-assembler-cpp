#include <cstdlib>
#include <sstream>
#include <iomanip>


class Account
{
private:
    const static size_t DATA_SIZE = 4;
    uint32_t data[DATA_SIZE];
public:
    Account() : data()
    {
    }

    Account(const std::string &str) : data()
    {
        if (str.substr(0, 2) == "0x") {
            std::string hex = str.substr(2);
            size_t i = 0;
            while (hex.length() > 0 && i < DATA_SIZE) {
                size_t split = hex.length() > 8 ? hex.length() - 8 : 0;
                std::stringstream(hex.substr(split)) >> std::hex >> data[i++];
                hex = hex.substr(0, split);
            }
        }
    }

    Account(const Account &other) : data()
    {
        for (size_t i = 0; i < DATA_SIZE; i++) {
            data[i] = other.data[i];
        }
    }

    Account(const uint32_t *raw) : data()
    {
        for (size_t i = 0; i < DATA_SIZE; i++) {
            data[i] = raw[i];
        }
    }

    operator std::string() const
    {
        if (!*this) {
            return "0";
        }

        std::stringstream stream;
        stream << "0x" << std::hex << std::setfill('0');
        for (ssize_t i = DATA_SIZE - 1; i >= 0; i--) {
            stream << std::setw(8) << data[i];
        }
        return stream.str();
    }

    operator bool() const
    {
        for (size_t i = 0; i < DATA_SIZE; i++) {
            if (data[i]) {
                return true;
            }
        }
        return false;
    }

    bool operator!() const
    {
        return !operator bool();
    }

    friend bool operator==(const Account &l, const Account &r);

    friend bool operator!=(const Account &l, const Account &r);

    friend bool operator<=(const Account &l, const Account &r);

    friend bool operator>=(const Account &l, const Account &r);

    friend bool operator<(const Account &l, const Account &r);

    friend bool operator>(const Account &l, const Account &r);

    size_t get_hash() const
    {
        size_t hash = 0;
        for (size_t i = 0; i < DATA_SIZE; i++) {
            hash ^= data[i];
        }
        return hash;
    }

    std::string to_string() const
    {
        return *this;
    }

    const uint32_t *cdata() const
    {
        return data;
    }
};


bool operator==(const Account &l, const Account &r)
{
    for (size_t i = 0; i < Account::DATA_SIZE; i++) {
        if (l.data[i] != r.data[i]) {
            return false;
        }
    }
    return true;
}

bool operator!=(const Account &l, const Account &r)
{
    return !(l == r);
}

bool operator<(const Account &l, const Account &r)
{
    for (ssize_t i = Account::DATA_SIZE - 1; i >= 0; i--) {
        if (l.data[i] < r.data[i]) {
            return true;
        } else if (l.data[i] > r.data[i]) {
            return false;
        }
    }
    return false;
}

bool operator>(const Account &l, const Account &r)
{
    return r < l;
}

bool operator<=(const Account &l, const Account &r)
{
    return !(r < l);
}

bool operator>=(const Account &l, const Account &r)
{
    return !(l < r);
}


namespace std
{
    template<>
    struct hash<Account>
    {
        size_t operator()(const Account &v) const
        {
            return v.get_hash();
        }
    };
}