#include <vector>
#include <iostream>


void process(std::vector<int> &v)
{
    std::vector<int>::iterator it = v.begin();
    while (it != v.end()) {
        it = v.erase(it);
        it += (it != v.end());
    }

    std::vector<int>::reverse_iterator rit = v.rbegin();
    while (rit != v.rend()) {
        std::cout << *rit << std::endl;
        rit++;
    }
}