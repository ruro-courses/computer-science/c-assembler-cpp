#include <utility>
#include <thread>


template<class F1, class F2, class... Args>
void async_launch(F1 &&f1, F2 &&f2, Args &&...args)
{
    std::thread{
            [](F1 &&_f1, F2 &&_f2, Args &&..._args)
            {
                std::forward<F2>(_f2)(std::forward<F1>(_f1)(std::forward<Args>(_args)...));
            },
            std::forward<F1>(f1),
            std::forward<F2>(f2),
            std::forward<Args>(args)...
    }.detach();
};


#include <iostream>
#include <functional>


int func(int &a, int &b)
{
    a += b;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    return a;
}

void print(int v)
{
    std::cout << v << std::endl;
    std::cout.flush();
}


int main()
{
    int x = 2, y = 6;
    async_launch(std::function(func), std::function(print), std::ref(x), y);
    //async_launch(func, print, std::ref(x), std::ref(y));
    std::this_thread::sleep_for(std::chrono::seconds(2));
}