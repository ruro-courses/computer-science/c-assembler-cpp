#include <list>
#include <vector>
#include <iterator>
#include <algorithm>


void process(const std::vector<int> &src, std::list<int> &dest)
{
    std::vector<int> sorted_unique_src(src);
    std::vector<int>::iterator begin = sorted_unique_src.begin(), end = sorted_unique_src.end();
    std::sort(begin, end);

    std::vector<int>::iterator unique = std::unique(begin, end);
    end = sorted_unique_src.erase(unique, end);

    std::list<int>::iterator dest_begin = dest.begin();
    int pos = 1;
    while (begin != sorted_unique_src.end() && dest_begin != dest.end()) {
        int target = *begin;
        if (target <= 0) {
            begin++;
            continue;
        }
        while (dest_begin != dest.end() && pos < target) {
            pos++;
            dest_begin++;
        }
        if (pos != target || dest_begin == dest.end()) {
            break;
        }
        dest_begin = dest.erase(dest_begin);

        pos++;
        begin++;
    }
}