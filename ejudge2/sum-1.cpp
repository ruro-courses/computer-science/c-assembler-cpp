#include <iostream>


class Sum
{
private:
    int accumulator;
public:
    Sum(int a, int b) : accumulator(0)
    {
        accumulator = a + b;
    }

    int get()
    {
        return accumulator;
    }
};
