#include <iostream>

// MEM_O -> [01]* 1 [01]^2

int get_char(char &tar)
{
    // 0  - input end
    // 1  - input ok
    // -1 - input was after word split
    int count = 0;
    do {
        if (!(std::cin >> std::noskipws >> tar)) {
            return 0;
        }
        count++;
    } while (std::isspace(tar));

    if (count != 1) {
        std::cin.putback(tar);
        return -1;
    }

    return 1;
}

enum State
{
    MEM_O, MEM_I, MEM_IO, MEM_II, MEM_IOO, MEM_IOI, MEM_IIO, MEM_III, ERROR
};

// MEM_O   -y-> MEM_y
// MEM_IOO -y-> MEM_y
// MEM_I   -y-> MEM_Iy
// MEM_IOI -y-> MEM_Iy
// MEM_Ix  -y-> MEM_Ixy
// MEM_IIx -y-> MEM_Ixy

// ANY_STATE    -INVALID CHARACTER-> ERROR
// ERROR        -ANY CHARACTER-> STOP
//     MEM_I??  -END OF WORD-> OK
// not MEM_I??  -END OF WORD-> ERROR

char current;
State state;
int ctrl = 1;

int check_state()
{
    return state == MEM_IOO || state == MEM_IOI || state == MEM_IIO || state == MEM_III;
}

void reset_state()
{
    current = ' ';
    state = MEM_O;
}

void state_change(State I, State O)
{
    if (current == '1') {
        state = I;
    } else {
        state = O;
    }
}

int main()
{
    reset_state();
    while (ctrl) {
        ctrl = get_char(current);

        // ERROR, skip to next word/end.
        if (state == ERROR) {
            while (ctrl == 1) {
                ctrl = get_char(current);
            }
        }

        // Next word/end.
        if (ctrl == -1 || !ctrl) {
            std::cout << check_state() << std::endl;
            reset_state();
            continue;
        }

        // Unexpected character.
        if (current != '0' && current != '1') {
            state = ERROR;
            continue;
        }

        switch (state) {
            case MEM_O:
                state_change(MEM_I, MEM_O);
                break;

            case MEM_I:
                state_change(MEM_II, MEM_IO);
                break;

            case MEM_IO:
                state_change(MEM_IOI, MEM_IOO);
                break;

            case MEM_II:
                state_change(MEM_III, MEM_IIO);
                break;

            case MEM_IOO:
                state_change(MEM_I, MEM_O);
                break;

            case MEM_IOI:
                state_change(MEM_II, MEM_IO);
                break;

            case MEM_IIO:
                state_change(MEM_IOI, MEM_IOO);
                break;

            case MEM_III:
                state_change(MEM_III, MEM_IIO);
                break;

            default:
                break;
        }
    }
}