#include <list>
#include <vector>
#include <iostream>
#include <iterator>


void process(const std::vector<int> &src, std::list<int> &dest, int step)
{
    std::vector<int>::const_iterator it_src = src.begin();
    std::list<int>::iterator it_dest = dest.begin();
    while (it_src != src.end() && it_dest != dest.end()) {
        *it_dest = *it_src;
        for (int i = 0; i < step && it_src != src.end(); i++, it_src++);
        it_dest++;
    }

    std::list<int>::reverse_iterator it_rev = dest.rbegin();
    for (; it_rev != dest.rend(); it_rev++) {
        std::cout << *it_rev << std::endl;
    }
}