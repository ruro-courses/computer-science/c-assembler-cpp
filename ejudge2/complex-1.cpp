#include <cstdio>
#include <cmath>


namespace numbers
{
    class complex
    {
    private:
        double re, im;
    public:
        complex(double real = 0, double imaginary = 0) : re(real), im(imaginary)
        {

        }

        explicit complex(const char *str) : re(0), im(0)
        {
            sscanf(str, "(%lf,%lf)", &re, &im);
        }

        double get_re() const
        {
            return re;
        }

        double get_im() const
        {
            return im;
        }

        double abs2() const
        {
            return im * im + re * re;
        }

        double abs() const
        {
            return sqrt(abs2());
        }

        void to_string(char *buf, size_t size) const
        {
            snprintf(buf, size, "(%.10g,%.10g)", re, im);
        }

        friend complex operator+(const complex &l, const complex &r);

        friend complex operator-(const complex &l, const complex &r);

        friend complex operator*(const complex &l, const complex &r);

        friend complex operator/(const complex &l, const complex &r);

        complex operator~() const
        {
            return complex(re, -im);
        }

        complex operator-() const
        {
            return complex(-re, -im);
        }
    };


    complex operator+(const complex &l, const complex &r)
    {
        return complex(l.re + r.re, l.im + r.im);
    }

    complex operator-(const complex &l, const complex &r)
    {
        return complex(l.re - r.re, l.im - r.im);
    }

    complex operator*(const complex &l, const complex &r)
    {
        return complex(l.re * r.re - l.im * r.im, l.re * r.im + l.im * r.re);
    }

    complex operator/(const complex &l, const complex &r)
    {
        double denom = r.re * r.re + r.im * r.im;
        return complex((l.re * r.re + l.im * r.im) / denom,
                (l.im * r.re - l.re * r.im) / denom);
    }
}