#include <bits/exception.h>
#include <iostream>


class DestructionPrinter
{
private:
    int value;
public:
    DestructionPrinter(int val)
            : value(val)
    {
    }

    ~DestructionPrinter()
    {
        std::cout << value << std::endl;
    }
};

void recursivePrintReverser()
{
    int val = 0;
    if (std::cin >> val) {
        DestructionPrinter dp(val);
        recursivePrintReverser();
    } else {
        throw std::exception();
    }
}

int main()
{
    try {
        recursivePrintReverser();
    } catch (std::exception &) {
    }
}