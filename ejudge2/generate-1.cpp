#include <iostream>

// S -> aSd | aId
// I -> bIc | bc

void print_seq(size_t K, size_t m)
{
    std::cout
            << std::string(m, 'a') << std::string(K - m, 'b')
            << std::string(K - m, 'c') << std::string(m, 'd')
            << std::endl;
}

void rec(size_t K, size_t m)
{
    print_seq(K, m);
    if (m > 1) {
        rec(K, m - 1);
    }
}

int main()
{
    size_t K;
    std::cin >> K;
    if (!(K % 2) && (K >= 4)) {
        rec(K / 2, K / 2 - 1);
    }
}