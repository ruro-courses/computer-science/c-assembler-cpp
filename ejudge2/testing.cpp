#include <iostream>
#include <sstream>


void test(std::istream inp)
{
    std::string buf;
    std::getline(inp, buf);
    std::cout << buf << std::endl;
}

int main()
{
    std::istringstream isp(std::string("10"));
    test(isp);
}
