#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>


class Accumulator
{
private:
    double sum;
    size_t count;
public:
    Accumulator() : sum(0), count(0)
    {
    }

    Accumulator &operator()(double value)
    {
        sum += value;
        count++;
        return *this;
    }

    double getAverage()
    {
        return count ? sum / count : 0;
    }
};


int main()
{
    std::vector<double> data;
    double buf;
    while (std::cin >> buf) {
        data.push_back(buf);
    }

    // Cut 10 pcent on each side.
    size_t border = data.size() / 10;
    data.erase(data.begin(), data.begin() + border);
    data.erase(data.end() - border, data.end());

    std::sort(data.begin(), data.end());

    // Cut 10 pcent on each side.
    border = data.size() / 10;
    data.erase(data.begin(), data.begin() + border);
    data.erase(data.end() - border, data.end());

    std::cout
            << std::setprecision(11)
            << std::for_each(data.begin(), data.end(), Accumulator()).getAverage()
            << std::endl;
}