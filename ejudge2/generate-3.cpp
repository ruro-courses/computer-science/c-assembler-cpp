#include <iostream>


void print_seq(std::string &buf, size_t pos, size_t split, size_t len)
{
    if (pos == len) {
        std::cout << buf << std::endl;
    } else if (pos < len - split) {
        buf[pos] = '1';
        buf[2 * (len - split) - pos - 1] = '1';
        print_seq(buf, pos + 1, split, len);

        buf[pos] = '2';
        buf[2 * (len - split) - pos - 1] = '2';
        print_seq(buf, pos + 1, split, len);
    } else {
        buf[(len - split) + pos] = '3';
        buf[2 * len - (pos - (len - split) + 1)] = '3';
        print_seq(buf, pos + 1, split, len);

        buf[(len - split) + pos] = '4';
        buf[2 * len - (pos - (len - split) + 1)] = '4';
        print_seq(buf, pos + 1, split, len);
    }

}


int main()
{
    size_t K;
    std::cin >> K;
    std::string buf = std::string(K, '*');
    K /= 2;
    for (size_t split = 0; split <= K; split++) {
        std::cout << split << std::endl;
        print_seq(buf, 0, split, K);
    }
}
