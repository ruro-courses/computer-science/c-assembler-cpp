
template <typename T>
typename T::value_type process(const T &v)
{
    typename T::value_type sum = typename T::value_type();
    int count = 0;
    for (typename T::const_reverse_iterator it = v.rbegin();
    it != v.rend() && count < 3; ++it, ++count) {
        sum += *it;
    }
    return sum;
}
