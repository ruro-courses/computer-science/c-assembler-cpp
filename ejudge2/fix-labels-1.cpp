#include <iostream>
#include <iterator>
#include <vector>
#include <map>


int main()
{
    // Read input.
    std::istream_iterator<std::string> input_start(std::cin);
    std::istream_iterator<std::string> input_end;
    std::vector<std::string> RPN(input_start, input_end);

    // Record labels.
    size_t position = 1;
    std::map<std::string, size_t> labels;
    for (std::vector<std::string>::iterator it = RPN.begin(); it != RPN.end();) {
        if (it->back() == ':') {
            it->pop_back();
            labels.insert(std::make_pair(*it, position));
            it = RPN.erase(it);
        } else {
            position++;
            it++;
        }
    }

    // Replace labels and print.
    for (std::string &label : RPN) {
        if (labels.count(label)) {
            std::cout << labels[label] << std::endl;
        } else {
            std::cout << label << std::endl;
        }
    }
}