#include <iostream>


void encode(char c, int n)
{
    std::cout << '#' << c << std::hex << n << '#';
}

void repeat(char c, int n)
{
    while (n--) {
        std::cout << c;
    }
}

int main()
{
    char last = '\0', current = '\0';
    int count = 0;
    bool end = false;

    while (!end) {
        if (!(std::cin >> std::noskipws >> current)) {
            end = true;
            current = '\0';
        }

        if (!last || current == last) {
            count++;
        } else {
            if (last == '#' || count > 4) {
                encode(last, count);
            } else {
                repeat(last, count);
            }
            count = 1;
        }
        last = current;
    }
}