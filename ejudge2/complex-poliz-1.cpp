#include <cstdio>
#include <cmath>


namespace numbers
{
    class complex
    {
    private:
        double re, im;
    public:
        complex(double real = 0, double imaginary = 0) : re(real), im(imaginary)
        {

        }

        explicit complex(const char *str) : re(0), im(0)
        {
            sscanf(str, "(%lf,%lf)", &re, &im);
        }

        double get_re() const
        {
            return re;
        }

        double get_im() const
        {
            return im;
        }

        double abs2() const
        {
            return im * im + re * re;
        }

        double abs() const
        {
            return sqrt(abs2());
        }

        void to_string(char *buf, size_t size) const
        {
            snprintf(buf, size, "(%.10g,%.10g)", re, im);
        }

        friend complex operator+(const complex &l, const complex &r);

        friend complex operator-(const complex &l, const complex &r);

        friend complex operator*(const complex &l, const complex &r);

        friend complex operator/(const complex &l, const complex &r);

        complex operator~() const
        {
            return complex(re, -im);
        }

        complex operator-() const
        {
            return complex(-re, -im);
        }
    };


    complex operator+(const complex &l, const complex &r)
    {
        return complex(l.re + r.re, l.im + r.im);
    }

    complex operator-(const complex &l, const complex &r)
    {
        return complex(l.re - r.re, l.im - r.im);
    }

    complex operator*(const complex &l, const complex &r)
    {
        return complex(l.re * r.re - l.im * r.im, l.re * r.im + l.im * r.re);
    }

    complex operator/(const complex &l, const complex &r)
    {
        double denom = r.re * r.re + r.im * r.im;
        return complex((l.re * r.re + l.im * r.im) / denom,
                (l.im * r.re - l.re * r.im) / denom);
    }

    void copyData(complex *dest, const complex *src, size_t length)
    {
        for (size_t i = 0; i < length; i++) {
            dest[i] = src[i];
        }
    }


    class complex_stack
    {
    private:
        complex *memory;
        size_t max_memory;
        size_t elem_count;

    public:
        complex pop()
        {
            elem_count--;
            return memory[elem_count];
        }

        void push(const complex &element)
        {
            if (elem_count + 1 > max_memory) {
                max_memory = 2 * max_memory + 1;
                *this = complex_stack(*this);
            }

            memory[elem_count] = element;
            elem_count++;
        }

        complex_stack(size_t starting_size = 0) :
                memory(new complex[starting_size]),
                max_memory(starting_size),
                elem_count(0)
        {
        }

        complex_stack(const complex_stack &other) :
                memory(new complex[other.max_memory]),
                max_memory(other.max_memory),
                elem_count(other.elem_count)
        {
            copyData(memory, other.memory, elem_count);
        }

        complex_stack &operator=(const complex_stack &other)
        {
            if (this == &other) {
                return *this;
            }

            max_memory = other.max_memory;
            elem_count = other.elem_count;

            delete[] memory;
            memory = new complex[max_memory];
            copyData(memory, other.memory, elem_count);

            return *this;
        }

        ~complex_stack()
        {
            delete[] memory;
        }

        complex_stack operator<<(const complex &elem) const
        {
            complex_stack copy = *this;
            copy.push(elem);
            return copy;
        }

        complex_stack operator~() const
        {
            complex_stack copy = *this;
            copy.pop();
            return copy;
        }

        complex operator[](size_t index) const
        {
            return memory[index];
        }

        complex operator+() const
        {
            return (*this)[elem_count - 1];
        }

        size_t size() const
        {
            return elem_count;
        }
    };


    complex eval(char **args, const complex &z)
    {
        complex_stack st;
        while (*args) {
            switch (**args) {
                case 'z': {
                    st.push(z);
                    break;
                }
                case '+': {
                    complex temp = st.pop();
                    st.push(st.pop() + temp);
                    break;
                }
                case '-': {
                    complex temp = st.pop();
                    st.push(st.pop() - temp);
                    break;
                }
                case '*': {
                    complex temp = st.pop();
                    st.push(st.pop() * temp);
                    break;
                }
                case '/': {
                    complex temp = st.pop();
                    st.push(st.pop() / temp);
                    break;
                }
                case '!': {
                    st.push(+st);
                    break;
                }
                case ';': {
                    st.pop();
                    break;
                }
                case '~': {
                    st.push(~st.pop());
                    break;
                }
                case '#': {
                    st.push(-st.pop());
                    break;
                }
                default: {
                    st.push(complex(*args));
                    break;
                }
            }
            args++;
        }
        return st.pop();
    }
}