#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

// return = c[i,j] = min(a[i,k] + b[k,j])
double elemMul(double *A, double *B, size_t i, size_t j, size_t N)
{
    double min = A[i] + B[j*N];
    double val;
    for (size_t k = 0; k < N; k++) {
        val = A[i + k*N] + B[k + j*N];
        min = val < min ? val : min;
    }
    return min;
}

// C = A*B
void mul(double *C, double *A, double *B, size_t N)
{
    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < N; j++) {
            C[i + j*N] = elemMul(A, B, i, j, N);
        }
    }
}

// C = A*A
void square(double *C, double *A, size_t N)
{
    // Bottom half
    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < i; j++) {
            C[i + j*N] = elemMul(A, A, i, j, N);
        }
    }
    // Top half
    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < i; j++) {
            C[j + i*N] = C[i + j*N];
        }
    }
}

// I = identity matrix init
void identity(double *I, size_t N)
{
    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < N; j++) {
            I[i + j*N] = i == j ? 0.0 : +INFINITY;
        }
    }
}

enum { PROT_RW = PROT_READ | PROT_WRITE };
enum { MAP_PA = MAP_PRIVATE | MAP_ANONYMOUS };
enum { O_RWCRTR = O_RDWR | O_CREAT | O_TRUNC };
enum { S_ALL = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH };

int main(int argc, char *argv[])
{
    int in = open(argv[1], O_RDONLY);
    int out = open(argv[2], O_RWCRTR, S_ALL);
    unsigned pow;
    sscanf(argv[3], "%u", &pow);

    struct stat fileinfo;
    fstat(in, &fileinfo);
    ssize_t filesize = fileinfo.st_size;
    size_t N = (size_t) sqrt((double)(filesize/sizeof(double)));

    double *mat = mmap(NULL, filesize, PROT_RW, MAP_PRIVATE, in, 0);
    double *buf = mmap(NULL, filesize, PROT_RW, MAP_PA, -1, 0);
    double *acc = mmap(NULL, filesize, PROT_RW, MAP_PA, -1, 0);
    identity(acc, N);
    double *tmp;

    while (pow) {
        if (pow & 1) { // acc *= mat
            mul(buf, acc, mat, N);
            tmp = buf;
            buf = acc;
            acc = tmp;
        }
        if (pow >>= 1) { // mat *= mat
            square(buf, mat, N);
            tmp = buf;
            buf = mat;
            mat = tmp;
        }
    }

    ftruncate(out, filesize);
    double *trg = mmap(NULL, filesize, PROT_RW, MAP_SHARED, out, 0);
    memcpy(trg, acc, filesize);
    msync(trg, filesize, MS_SYNC);
}
