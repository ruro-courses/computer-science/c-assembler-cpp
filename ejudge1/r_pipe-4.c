#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>

size_t childcount = 0;
size_t lastchild = 0;
pid_t *childlist = NULL;

void shutdown(bool error)
{
    for (size_t i = 0; i < lastchild; i++) {
        if (error) {
            kill(childlist[i], SIGKILL);
        }
        while (waitpid(childlist[i], NULL, 0) != -1 && errno != ECHILD);
    }
    exit(error);
}

void cloexec(int fd)
{
    if (fcntl(fd, F_SETFD, FD_CLOEXEC))
        shutdown(true);
}

typedef struct
{
    int in;
    int out;
} Pipe;

Pipe getPipe(void)
{
    int fd[2];
    if (pipe(fd))
        shutdown(true);
    cloexec(fd[0]);
    cloexec(fd[1]);
    return (Pipe){ .in = fd[0], .out = fd[1] };
}

void process(int in, const char *cmd, int out)
{
    if ((childlist[lastchild] = fork()) == -1) {
        shutdown(true);
    }
    if (!childlist[lastchild]) {
        dup2(in, STDIN_FILENO);
        dup2(out, STDOUT_FILENO);
        execlp(cmd, cmd, NULL);
        exit(0);
    }
    lastchild++;
}

int main(int argc, char *argv[])
{
    childcount = argc - 1;
    childlist = calloc(childcount, sizeof(*childlist));
    if (!childlist) {
        shutdown(true);
    }

    Pipe prev, next;
    prev.in = dup(STDIN_FILENO);
    if (prev.in == -1) {
        shutdown(true);
    }

    while (*++argv) {
        if (argv[1]) {
            next = getPipe();
        } else {
            next.out = STDOUT_FILENO;
        }
        process(prev.in, *argv, next.out);
        close(prev.in);
        close(next.out);
        prev = next;
    }
    free(childlist);
    shutdown(false);
}
