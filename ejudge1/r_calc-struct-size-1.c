#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

typedef struct
{
    char *name;
    size_t size;
    size_t align;
} Type;

char *getNewLine(void)
{
    char *str = NULL;
    size_t N = 0;
    getline(&str, &N, stdin);
    if (!*str) {
        free(str);
        return NULL;
    }
    return str;
}

char *parseName(char *str)
{
    while (*str && !isspace(*str)) {
        str++;
    }
    *str++ = '\0';
    return str;
}

int main(int argc, char *argv[])
{
    Type *types = NULL;
    size_t num = 0;
    while (1) {
        char *str = getNewLine();
        if (!strcmp(str, "END\n")) {
            free(str);
            break;
        }
        num++;
        types = realloc(types, sizeof(*types)*num);
        types[num-1].name = str;
        str = parseName(str);
        sscanf(str, "%zu %zu\n", &types[num-1].size, &types[num-1].align);
    }
    size_t strct_size = 0;
    size_t strct_align = 0;
    bool overflow = false;
    while (1) {
        char *name = getNewLine();
        if (!name) {
            break;
        }
        char *data = parseName(name);
        size_t i;
        for (i = 0; i < num; i++) {
            if (!strcmp(types[i].name, name)) {
                break;
            }
        }
        size_t array_len = 0;
        sscanf(data, "%zu\n", &array_len);
        size_t elem_align = types[i].align;
        size_t align_break = strct_size % elem_align;
        size_t elem_size = align_break ? elem_align : 0;
        size_t tmp;
        if (__builtin_add_overflow(elem_size, -align_break, &elem_size) ||
                __builtin_mul_overflow(types[i].size, array_len, &tmp) ||
                __builtin_add_overflow(elem_size, tmp, &elem_size) ||
                __builtin_add_overflow(strct_size, elem_size, &strct_size)) {
            strct_align = 0;
            strct_size = 0;
            overflow = true;
            break;
        }
        
        strct_align = (strct_align < elem_align) ? elem_align : strct_align;
    }
    if (!overflow) {
        if (!strct_align) {
            strct_align = 1;
            strct_size = 1;
        } else {
            size_t align_break = strct_size % strct_align;
            if(__builtin_add_overflow(strct_size, align_break ?
                    strct_align - align_break : 0, &strct_size)) {
                strct_size = 0;
                strct_align = 0;
            }
        }
    }
    printf("%zu %zu\n", strct_size, strct_align);
    for (size_t i = 0; i < num; i++) {
        free(types[i].name);
    }
    free(types);
}
