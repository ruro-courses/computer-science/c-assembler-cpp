#include <stdlib.h>
#include <stdio.h>

double randf(void)
{
    double res = rand();
    res /= (double)RAND_MAX + 1;
    return res;
}

int main(int argc, char *argv[])
{
    long long count = strtoll(argv[1], NULL, 10);
    long long low = strtoll(argv[2], NULL, 10);
    long long high = strtoll(argv[3], NULL, 10);
    long seed = strtol(argv[4], NULL, 10);
    long long temp;
    srand(seed);
    while (count--)
    {
        temp = low + (long long)(randf()*(high-low));
        printf("%lld\n", temp);
    }
}
