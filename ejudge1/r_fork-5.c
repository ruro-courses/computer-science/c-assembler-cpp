#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void child(int id, int max)
{
    printf("%d%c", id, (id == max) ? '\n' : ' ');
    fflush(stdout);
    
    if (id != max && !fork())
    {
        child(id + 1, max);
    }
    while (wait(NULL) != -1);
    exit(0);
}

int main(void)
{
    int N;
    scanf("%d", &N);
    child(1, N);
}
