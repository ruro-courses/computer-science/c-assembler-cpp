#include <unistd.h>
#include <errno.h>

enum { BUFFER_SIZE = 4096 };

void copy_file(int in_fd, int out_fd)
{
    char buf[BUFFER_SIZE];
    ssize_t left, done, temp;
    while ((temp = read(in_fd, buf, BUFFER_SIZE))) {
        if (temp < 0 && errno == EINTR) { continue; }
        done = 0;
        left = temp;
        do {
            temp = write(out_fd, buf + done, left);
            if (temp < 0 && errno == EINTR) { continue; }
            done += temp;
            left -= temp;
        } while (left);
    }
}
