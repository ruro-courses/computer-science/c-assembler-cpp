#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

#include "plugin.h"

void error(const char *msg)
{
    int errval = errno;
    fprintf(stderr, "%s\n", msg);
    if (errval) {
        fprintf(stderr, "Error: %s\n", strerror(errval));
    }
    exit(1);
}

enum { STRTOL_BASE = 10 };

int getarg(char *str)
{
    char *chk;
    errno = 0;
    long ret = strtol(str, &chk, STRTOL_BASE);
    if (!*str || *chk || errno || ret > INT_MAX || ret < INT_MIN) {
        error("Invalid number.");
    }
    return ret;
}

enum { MAX_NAME_LEN = 64 };

int main(int argc, char *argv[])
{
    if (argc != 7) {
        error("Wrong number of arguments!");
    }
    void *handle = NULL;
    struct RandomFactory *(*con)(void) = NULL;
    struct RandomFactory *fac = NULL;
    struct RandomGenerator *gen = NULL;
    int a, b, count;
    int len;
    char buf[MAX_NAME_LEN];
    handle = dlopen(argv[1], RTLD_NOW);
    if (!handle) {
        error("Couldn't open plugin.");
    }
    if (!*argv[2] || (len = snprintf(buf, sizeof(buf),
            "random_%s_factory", argv[2])) <= 0 || len >= MAX_NAME_LEN) {
        error("Couldn't parse factory name.");
    }
    *((void **)&con) = dlsym(handle, buf);
    if (!con) {
        error(dlerror());
    }
    fac = con();
    if (!fac || !fac->ops) {
        error("Factory creation failed.");
    }
    gen = fac->ops->new_instance(fac, argv[3]);
    if (!gen || !gen->ops) {
        error("Generator creation failed.");
    }
    count = getarg(argv[4]);
    a = getarg(argv[5]);
    b = getarg(argv[6]);
    if (a > b || count > 1000 || count < 0) {
        error("Couldn't parse numberical arguments.");
    }
    for (int i = 0; i < count; i++) {
        printf("%d\n", gen->ops->next_int(gen, a, b));
    }
    gen->ops->destroy(gen);
    fac->ops->destroy(fac);
    dlclose(handle);
}
