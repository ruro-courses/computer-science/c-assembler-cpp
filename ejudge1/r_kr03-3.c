#include <stdio.h>
#include <unistd.h>
#include <endian.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>

typedef struct
{
    int32_t key;
    int32_t val;
} KeyVal;

bool getKV(int fd, ssize_t offset, KeyVal *ret)
{
    KeyVal buf;
    lseek(fd, offset*sizeof(buf), SEEK_SET);
    if (read(fd, &buf, sizeof(buf)) != sizeof(buf))
        return false;
    ret->key = be32toh(buf.key);
    ret->val = be32toh(buf.val);
    return true;
}

int main(int argc, char *argv[])
{
    int file;
    int32_t searchkey;
    ssize_t start, end, mid;
    KeyVal buf;
    struct stat fileinfo;
    file = open(argv[1], O_RDONLY);
    sscanf(argv[2], "%"SCNd32, &searchkey);
    fstat(file, &fileinfo);
    start = 0;
    end = fileinfo.st_size/sizeof(buf);
    mid = (end-start)/2;
    while (getKV(file, mid, &buf) && start < end) {
        if (buf.key == searchkey) {
            printf("1 %"PRId32"\n", buf.val);
            return 0;
        }
        if (buf.key < searchkey) {
            start = mid+1;
        }
        if (buf.key > searchkey) {
            end = mid;
        }
        mid = start + (end-start)/2;
    }
    printf("0 0\n");
    return 0;
}
