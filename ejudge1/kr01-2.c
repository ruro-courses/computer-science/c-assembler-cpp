#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    while (*++argv) {
        int32_t temp;
        int32_t shift;
        int32_t offset = 0;
        int32_t max = INT32_MIN;
        int32_t min = INT32_MAX;
        while (sscanf(*argv + offset, "%d%n", &temp, &shift) == 1) {
            offset += shift;
            offset += (*(*argv + offset) == ',');
            if (temp > max) {
                max = temp;
            }
            if (temp < min) {
                min = temp;
            }
        }
        uint64_t res = max;
        res -= min;
        res += 1;
        printf("%llu\n", res);
    }
}
