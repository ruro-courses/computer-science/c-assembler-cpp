#include <time.h>
#include <stdio.h>
#include <stdint.h>

enum
{
    DATE_LEN = 4 + 2 + 2 + 3,
    SEC_DAY = 60 * 60 * 24
};

int main(void)
{
    int32_t next;
    char buf[DATE_LEN];
    while (scanf("%d", &next) == 1) {
        // next *= SEC_DAY;
        if (__builtin_mul_overflow(next, SEC_DAY, &next)) {
            printf("OVERFLOW\n");
            continue;
        }
        // now += next;
        time_t now = time(NULL);
        if (__builtin_add_overflow(now, next, &now)) {
            printf("OVERFLOW\n");
            continue;
        }
        strftime(buf, sizeof(buf), "%Y-%m-%d", localtime(&now));
        printf("%s\n", buf);
    }
}
