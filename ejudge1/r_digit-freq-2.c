#include <stdio.h>

int
main(void)
{
    char filename[64]   = {};
    unsigned digits[10] = {};
    char digit          = '\0';
    FILE *input         = NULL;
    int len             = 0;
    
    scanf("%63[^\n]%n", filename, &len);
    input = fopen(filename, "r");
    
    if (input != NULL && len <= 61) {
        while ((digit = fgetc(input)) != EOF)
            if ('0' <= digit && digit <= '9')
                digits[digit - '0']++;
    }

    for (unsigned i=0; i <= 9; i++)
        printf("%u %u\n", i, digits[i]);
    
    return 0;
}
