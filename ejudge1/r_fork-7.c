#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

pid_t child(void)
{
    pid_t temp;
    int ret;
    if ((temp = fork()))
    {
        if (temp == -1)
        {
            return 1;
        }
        while (wait(&ret) != -1)
        {
            if (WIFEXITED(ret))
            {
                return WEXITSTATUS(ret);
            }
        }
        return 1;
    }
    
    int tmp;
    if (scanf("%d", &tmp) != 1)
    {
        exit(0);
    }

    if (child() == 1)
    {
        exit(1);
    }

    printf("%d\n", tmp);
    exit(0);
}

int main(void)
{
    if (child() == 1)
    {
        printf("-1\n");
    }
}
