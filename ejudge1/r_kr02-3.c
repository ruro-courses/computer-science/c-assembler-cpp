#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <stdio.h>

char **run(char **args)
{
    char **runp = args;
    while (*runp && strcmp(*runp, "END")) {
        runp++;
    }
    if (*runp) {
        *runp = NULL;
        runp++;
    }
    if (!fork()) {
        execvp(args[0], args);
        exit(1);
    }
    return runp;
}

int main(int argc, char **argv)
{
    int status, tmp;
    argv++;
    while (*argv) {
        argv = run(argv);
        errno = 0;
        while (wait(&tmp) != -1 && errno != ECHILD) {
            errno = 0;
            status = tmp;
        }
        if (!WIFEXITED(status) || WEXITSTATUS(status))
            return 1;
    }
}
