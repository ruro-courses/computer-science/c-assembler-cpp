#include <stdint.h>

// We know, they are the biggest and smallest values because (int32_t)
// is two's complement kind of signed value.
enum
{
    SAT_MAX = ~(uint32_t)0 >> 1,
    SAT_MIN = ~SAT_MAX
};

int32_t satsum(int32_t v1, int32_t v2)
{
    int32_t ret;
    if (__builtin_add_overflow(v1, v2, &ret))
    {
        // Overflow.
        // If v1 > 0 and v2 > 0 this was a positive overflow.
        // If v1 < 0 and v2 < 0 this was a negative overflow.
        // It is enough to check v1 to decide which.
        return (v1 > 0) ? SAT_MAX : SAT_MIN;
    }
    // No overflow.
    return ret;
}
