#include <stdint.h>
#include <stdio.h>
#include <malloc.h>

enum
{
    UTF_SINGLE = 0x80,
    UTF_NEXT = 0x40
};

enum
{
    UTF_CUTOFF = 0x20,
    DYN_START_BUF = 1 << 8,
    GETCHAR_BUF = 1 << 20,
};

size_t getword(uint8_t **buf, size_t oldlen)
{
    // Read a word from stdin into a dynamic array.
    // Words are delimited by character with value <= UTF_CUTOFF.
    size_t len = (*buf) ? oldlen : DYN_START_BUF;
    size_t off = 0;
    int c;
    uint8_t *str = (*buf) ? *buf : calloc(len, 1);
    while (1) {
        c = getchar_unlocked();
        
        // Input ended.
        if (c == EOF && !off) {
            free(str);
            *buf = NULL;
            return 0;
        }

        // Skip leading delimiters.
        if (c <= UTF_CUTOFF && !off) {
            continue;
        }

        // End of word.
        if (c == EOF || c <= UTF_CUTOFF) {
            str[off] = '\0';
            off++;
            break;
        }

        str[off] = c;
        off++;
        if (len == off) {
            len *= 2;
            str = realloc(str, len);
        }
    }
    *buf = str;
    return len;
}

size_t getlength(uint8_t *word)
{
    size_t wordlen = 0;
    while (*word) {
        // Everything that's not 10xxxxxx is a start of a new code point.
        if (!(*word & UTF_SINGLE) || (*word & UTF_NEXT)) {
            wordlen++;
        }
        word++;
    }
    return wordlen;
}

int main(void)
{
    uint8_t *max_word = NULL;
    uint8_t *word = NULL;
    size_t old_size = 0;
    size_t max_len = 0;
    size_t len;

    // Set a bigger buffer for getchar.
    char *readbuf = malloc(GETCHAR_BUF);
    setvbuf(stdin, readbuf, _IOFBF, GETCHAR_BUF);

    // Process words.
    while ((old_size = getword(&word, old_size))) {
        len = getlength(word);
        if (len > max_len) {
            free(max_word);
            max_word = word;
            max_len = len;
            word = NULL;
            old_size = 0;
            continue;
        }
    }

    printf("%zu\n", max_len);
    if (max_len) {
        printf("%s\n", max_word);
    }

    // Clean up.
    free(max_word);
    free(readbuf);
}
