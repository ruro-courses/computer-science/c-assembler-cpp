#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

intmax_t max = 0;

void child(int id, int input, int output)
{
    if (!fork()) {
        FILE *inp = fdopen(input, "r");
        FILE *out = fdopen(output, "w");
        intmax_t local = 0;
        while (fscanf(inp, "%jd", &local) == 1 && local < max) {
            printf("%d %jd\n", id, local);
            fflush(stdout);

            fprintf(out, "%jd ", ++local);
            fflush(out);
        }
        fprintf(out, ".");
        fclose(out);
        fclose(inp);
        exit(0);
    }
}

int main(int argc, char *argv[])
{
    sscanf(argv[1], "%jd", &max);
    int pipes[2][2];
    pipe(pipes[0]);
    pipe(pipes[1]);

    child(1, pipes[0][0], pipes[1][1]);
    child(2, pipes[1][0], pipes[0][1]);

    close(pipes[0][0]);
    close(pipes[1][0]);
    close(pipes[1][1]);

    FILE *start = fdopen(pipes[0][1], "w");
    fprintf(start, "1 ");
    fflush(start);
    fclose(start);

    while (wait(NULL) != -1 || errno != ECHILD); 
    printf("Done\n");
    fflush(stdout);
}
