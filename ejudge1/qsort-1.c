#include <stdlib.h>

int compar(const void *a, const void *b, void *origin)
{
    // x and y are indeces of elements in the real array.
    int x = *(int *)a;
    int y = *(int *)b;
    const int *data = origin;
    // If the values in the real array match, sort based on index. (stable sort)
    if (data[x] == data[y])
        return (x > y) ? 1 : -1;
    // Otherwise sort based on value.
    return (data[x] > data[y]) ? 1 : -1;
}

void process(size_t count, const int *data, int *order)
{
    // Fill the order array with indeces to be sorted.
    for (size_t i = 0; i < count; i++) { order[i] = i; }
    // We pass (void *)data to qsort as a reference for sorting indeces.
    qsort_r(order, count, sizeof(*data), compar, (void *)data);
}
