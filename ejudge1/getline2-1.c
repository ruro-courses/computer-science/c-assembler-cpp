#include <string.h>
#include <malloc.h>

char *getline2(FILE *f)
{
    size_t len = 2;
    size_t off = 0;
    char *str = malloc(len);
    while (fgets(str + off, len - off, f)) {
        off = strnlen(str, len);
        if (str[off - 1] == '\n') {
            break;
        }
        len *= 2;
        str = realloc(str, len);
    }
    if (!off) {
        free(str);
        return NULL;
    }
    str = realloc(str, off + 1);
    return str;
}
