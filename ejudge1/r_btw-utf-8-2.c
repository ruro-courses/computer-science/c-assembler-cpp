#define _GNU_SOURCE
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

enum
{
    UTF_SINGLE = 0x80,
    UTF_NEXT = 0x40,
    UTF_DOUBLE = 0x20,
    UTF_TRIPLE = 0x10,
    UTF_QUAD = 0x08
};

enum
{
    UTF_CUTOFF = 0x20,
    DYN_START_BUF = 256
};

typedef struct
{
    uint8_t *start;
    uint8_t *end;
} PointRow;

uint8_t *getnextPoint(uint8_t *str)
{
    // Returns pointer to the next UTF-8 code point, or NULL if str is invalid.
    // See UTF-8 spec.
    if (!str) {
        return NULL;    // invalid
    } else if (!(*str & UTF_SINGLE)) {
        return str + 1; // 0xxxxxxxx (one byte)
    } else if (!(*str & UTF_NEXT)) {
        return NULL;    // 10xxxxxxx (middle of code point)
    } else if (!(*str & UTF_DOUBLE)) {
        return str + 2; // 110xxxxxx (two byte code point)
    } else if (!(*str & UTF_TRIPLE)) {
        return str + 3; // 1110xxxxx (three byte code point)
    } else if (!(*str & UTF_QUAD)) {
        return str + 4; // 11110xxxx (four byte code point)
    } else {
        return NULL;    // 11111???? (invalid code point)
    }
}

uint8_t *gettext(size_t *size)
{
    // Read everything from stdin into a dynamic array.
    // Ignores character with value <= UTF_CUTOFF.
    size_t len = DYN_START_BUF;
    size_t off = 0;
    uint8_t *str = calloc(len, 1);
    int c;
    while ((c = getchar()) != EOF) {
        if (c <= UTF_CUTOFF) {
            continue;
        }
        str[off] = c;
        off++;
        if (len == off) {
            len *= 2;
            str = realloc(str, len);
        }
    }
    if (!off) {
        free(str);
        return NULL;
    }
    str = realloc(str, off);
    *size = off;
    return str;
}

PointRow *getRows(uint8_t *start, size_t *size)
{
    // Parse text into UTF-8 code points. Store virtual Rows in a dynamic array.
    size_t len = DYN_START_BUF;
    size_t off = 0;
    uint8_t *t = start;
    PointRow *p = calloc(len, sizeof(*p));
    while ((t - start) < *size) {
        p[off].end = t;
        p[off].start = t = getnextPoint(t);
        off++;
        if (len == off) {
            len *= 2;
            p = realloc(p, len * sizeof(*p));
        }
    }
    p[off-1].start = start;
    p = realloc(p, off * sizeof(*p));
    *size = off;
    return p;
}

int cmpRows(const void *a, const void *b, void *c)
{
    // Lexicographically compare virtual Rows.
    size_t clen = *(size_t *)c;
    uint8_t *x = ((PointRow *)a)->start;
    uint8_t *y = ((PointRow *)b)->start;
    return memcmp(x, y, clen);
}

void putPoint(uint8_t *p)
{
    // Print all characters of a UTF-8 code point.
    fwrite(p, 1, getnextPoint(p) - p, stdout);
}

int main(void)
{
    size_t clen = 0;
    size_t plen = 0;
    uint8_t *t = NULL;
    PointRow *r = NULL;

    // Read from stdin, repeat text twice in buffer, so we don't have to bother
    // with indexing. O(n) memory.
    t = gettext(&clen);
    t = realloc(t, 2*clen);
    memmove(t + clen, t, clen);
    
    // Parse text into virtual rows. Each virtual row represents a row in the
    // Burrows-Wheeler transform matrix. O(n) memory.
    plen = clen;
    r = getRows(t, &plen);
    
    // Sort the rows lexicographically and print 'last' character in each row.
    // Assuming qsort is quicksort or similar.
    // O(nlog(n)) comparisons, each comparison is O(log(n)) or O(n) at worst.
    qsort_r(r, plen, sizeof(*r), cmpRows, &clen);
    for (size_t i = 0; i < plen; i++) {
        putPoint(r[i].end);
    }
    putchar('\n');
    
    // Clean up.
    free(t);
    free(r);
}
