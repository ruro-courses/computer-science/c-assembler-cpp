#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>

volatile sig_atomic_t counter = 0;

enum { MAX_COUNTER = 4 };

void handl(int sig)
{
    sig_atomic_t local = counter++;
    if (local >= MAX_COUNTER) {
        exit(0);
    }
    printf("%ju\n", (uintmax_t)local);
    fflush(stdout);
}

int main(void)
{
    sigset_t set;
    sigfillset(&set);
    struct sigaction act;
    act.sa_handler = handl;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);
    printf("%ju\n", (uintmax_t)getpid());
    fflush(stdout);
    while(pause());
}
