#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>

enum { proc_num = 3 };

void child(int id, int in)
{
    if (!fork())
    {
        // Child.
        if (id + 1 < proc_num) {
            child(id + 1, in);
        }
        // Wait for children to die.
        while (wait(NULL) != -1 || errno != ECHILD);
        // Get time.
        time_t val;
        struct tm time_st;
        while (read(in, &val, sizeof(val)) != sizeof(val));
        localtime_r(&val, &time_st);
        int t = -1;
        char c = '\0';
        switch (id)
        {
            case 0: //H
                t = time_st.tm_hour;
                c = 'H';
                break;
            case 1: //M
                t = time_st.tm_min;
                c = 'M';
                break;
            case 2: //S
                t = time_st.tm_sec;
                c = 'S';
                break;
            default: //?!?
                break;
        }
        printf("%c:%02d\n", c, t);
        fflush(stdout);
        exit(0);
    }
}

int main(void)
{
    time_t val;
    int fd[2];
    time(&val);
    pipe(fd);
    for (int i = 0; i < proc_num; i++) {
        write(fd[1], &val, sizeof(val));
    }
    child(0, fd[0]);
    while (wait(NULL) != -1 || errno != ECHILD);
}
