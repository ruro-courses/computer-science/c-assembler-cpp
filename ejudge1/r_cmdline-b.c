#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

enum
{
    S_IRWU = S_IRUSR | S_IWUSR,
    S_IRWG = S_IRGRP | S_IWGRP,
    S_IRWO = S_IROTH | S_IWOTH,
    S_IRWA = S_IRWU | S_IRWG | S_IRWO
};

int main(int argc, char *argv[])
{
    if (!fork()) {
        int inp = open(argv[2], O_RDONLY);
        int out = open(argv[3], O_WRONLY | O_CREAT | O_TRUNC, S_IRWA);
        dup2(inp, STDIN_FILENO);
        dup2(out, STDOUT_FILENO);
        close(inp);
        close(out);
        execlp(argv[1], argv[1], NULL);
    }
    while (wait(NULL) != -1);
    return 0;
}
