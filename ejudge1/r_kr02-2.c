#include <stdio.h>
#include <time.h>

enum { DAYS_IN_WEEK = 7 };
enum { START_YEAR = 1900 };

int main(void)
{
    int year, mon, wday;
    scanf("%d %d %d", &year, &mon, &wday);
    year -= START_YEAR;
    mon--;
    struct tm start = {0};
    start.tm_wday = wday;
    start.tm_mon = mon;
    start.tm_year = year;
    start.tm_mday = 0;
    do {
        start.tm_mday++;
        mktime(&start);
    } while (start.tm_wday != wday);
    do {
        printf("%d\n", start.tm_mday);
        start.tm_mday += DAYS_IN_WEEK;
    } while (mktime(&start) != -1 && start.tm_mon == mon);
}
