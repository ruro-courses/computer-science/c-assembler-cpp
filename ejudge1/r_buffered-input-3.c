#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

enum
{
    READ_SIZE = 16,
    NUM_BASE  = 10
};

int bread(char *buf, int left)
{
    // Reads a block of `left` bytes. If less than `left`
    // bytes is available, fills the rest with 0.
    // Returns the number of bytes actually read.
    size_t temp = 0;
    size_t done = 0;
    while ((temp = read(STDIN_FILENO, buf + done, left - done))) {
        done += (temp > 0) ? temp : 0;
    }
    memset(buf + done, 0, left - done);
    return done;
}

int main(void)
{
    long long sum = 0;
    int temp = 0;
    int sign = 1;
    char buf[READ_SIZE] = {0};
    while (bread(buf, sizeof(buf))) {
        for (int i = 0; i < sizeof(buf); i++) {
            if (isspace(buf[i]) || !buf[i]) {
                sum += temp*sign;
                temp = 0;
                sign = 1;
            } else if (buf[i] == '-') {
                sign = -1;
            } else if (buf[i] >=  '0'
                    && buf[i] <= ('0' + NUM_BASE)) {
                temp *= NUM_BASE;
                temp += buf[i] - '0';
            }
        }
    }
    printf("%lld\n", sum);
}
