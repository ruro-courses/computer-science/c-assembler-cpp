#include <stdio.h>

int main(void)
{
    unsigned char a[8] = {0};
    unsigned char b[8] = {0};
    while (scanf("%hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx",
                &a[0], &a[1], &a[2], &a[3],
                &a[4], &a[5], &a[6], &a[7]) == 8)
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                b[i] |= !!(a[j] & 1u << i) << (7-j);
            }
        }
        printf("%hhx %hhx %hhx %hhx %hhx %hhx %hhx %hhx\n",
                b[0], b[1], b[2], b[3],
                b[4], b[5], b[6], b[7]);
        for (int i = 0; i < 8; i++) {
            b[i] = 0;
        }
    }
}
