#include <stdio.h>

int
main (void)
{
    unsigned temp = 0;
    while (1 == scanf("%u", &temp)) {
        unsigned count = 0;
        // Skip until first bit.
        while (temp && !(temp & 1))
            temp >>= 1;
        // Count while there are bits left.
        while (temp) {
            count++;
            temp >>= 1;
        }
        // The mantissa is in form 1,a1a2...a22a23
        // (where aN are 23 stored bits + 1 implied in the beginning)
        printf("%u\n", count <= 24);
    }
    return 0;
}
