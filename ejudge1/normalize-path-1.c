void normalize_path(char *buf)
{
    if (!buf) {
        return;
    }
    char *run = buf;
    while (*run) {
        *buf = *run;
        buf++;
        if (*run != '/') {
            run++;
            continue;
        }
        while (*run == '/') {
            run++;
        }
    }
    *buf = '\0';
}
