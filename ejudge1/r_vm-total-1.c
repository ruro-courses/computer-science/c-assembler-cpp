#include <stdio.h>
#include <inttypes.h>

int main(void)
{
    uint32_t start, finish, total = 0;
    while (scanf("%x-%x%*[^\n]\n", &start, &finish) == 2)
    {
        total += finish - start;
    }
    printf("%u\n", total);
}
