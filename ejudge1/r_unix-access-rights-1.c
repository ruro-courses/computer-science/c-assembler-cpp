#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>

enum
{
    LINE_LENGTH = 1021,
    MAX_GID_NUM = 32
};

enum
{
    STRTO_ID = 10,
    STRTO_PERM = 8
};

enum
{
    PERM_OFF_OTH = 0,
    PERM_OFF_GRP = 3,
    PERM_OFF_USR = 6
};

typedef struct
{
    uid_t uid;
    gid_t gid;
    int perm;
    char *name;
} FileStat;

typedef struct
{
    uid_t uid;
    gid_t gid[MAX_GID_NUM];
    int gid_num;
    int perm_ask;
} UserStat;

void read_user_stat(UserStat *, char *);
void read_file_stat(FileStat *, char *);
int check_perm(FileStat *, UserStat *);
int is_gid_in_arr(gid_t, gid_t *, int);

int main(void)
{

    char buf[LINE_LENGTH];
    UserStat user;
    FileStat file;

    read_user_stat(&user, buf);

    while (fgets(buf, sizeof(buf), stdin)) {
        read_file_stat(&file, buf);
        if (check_perm(&file, &user)) {
            printf("%s\n", file.name);
        }
    }
}

void read_user_stat(UserStat *stat, char *buf)
{
    // Read UID
    fgets(buf, LINE_LENGTH, stdin);
    stat->uid = strtol(buf, NULL, STRTO_ID);
    
    // Read GID, until EOL or unparsable char.
    fgets(buf, LINE_LENGTH, stdin);
    char *run = buf;
    char *end = buf;
    int i;
    for (i = 0; i < MAX_GID_NUM; i++) {
        stat->gid[i] = strtol(run, &end, STRTO_ID);
        if (run == end) {
            break;
        }
        run = end;
    }
    stat->gid_num = i;
    
    // Read permissions, the user is asking for.
    fgets(buf, LINE_LENGTH, stdin);
    stat->perm_ask = strtol(buf, NULL, STRTO_PERM);
}

void read_file_stat(FileStat *file, char *buf)
{
    file->uid = strtol(buf, &buf, STRTO_ID);
    file->gid = strtol(buf, &buf, STRTO_ID);
    file->perm = strtol(buf, &buf, STRTO_PERM);
    
    // Cut whitespace in the filename at the start.
    while (isspace(*++buf));
    file->name = buf;

    // Cut whitespace in the filename at the end.
    buf += strlen(buf);
    while (isspace(*--buf));
    *(buf + 1) = '\0';
}

int check_perm(FileStat *file, UserStat *user)
{
    // Set the offset of the access rights field for user, group or other. 
    int perm_off;
    if (user->uid == file->uid) {
        perm_off = PERM_OFF_USR;
    } else if (is_gid_in_arr(file->gid, user->gid, user->gid_num)) {
        perm_off = PERM_OFF_GRP;
    } else {
        perm_off = PERM_OFF_OTH;
    }
    
    // Check if permissions are at least perm_ask or more.
    return user->perm_ask == (file->perm >> perm_off & user->perm_ask);
}

int is_gid_in_arr(gid_t gid, gid_t *arr, int size)
{
    for (int i = 0; i < size; i++) {
        if (gid == arr[i]) {
            return 1;
        }
    }
    return 0;
}
