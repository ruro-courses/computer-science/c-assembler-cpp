#include <stdio.h>
#include <stdint.h>

enum
{
    OCTET_MASK = 0x7F,
    OCTET_SIZE = 7
};

int main(void)
{
    uint32_t temp;
    uint8_t octet;
    while (scanf("%u", &temp) == 1) {
        do {
            octet = temp & OCTET_MASK;
            temp >>= OCTET_SIZE;
            if (temp) {
                octet |= 1 << OCTET_SIZE;
                printf("%02x ", octet);
            } else {
                printf("%02x", octet);
            }
        } while (temp);
        printf("\n");
    }
}
