#include <stdio.h>
#include <stdlib.h>

enum
{
    MOD_BASE = 8,
    MOD_START = 8
};

int main(int argc, char *argv[])
{
    const char access[] = "xwr";
    while (*++argv)
    {
        unsigned long rights = strtoul(*argv, NULL, MOD_BASE);
        for (int i = MOD_START; i >= 0; i--) {
            printf("%c", (rights&(1<<i)) ? access[i%3] : '-');
        }
        printf("\n");
    }
}
