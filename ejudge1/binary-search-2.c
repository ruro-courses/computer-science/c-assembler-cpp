#include <sys/types.h>

int bsearch2(const void *key,
    const void *base,
    ssize_t nmemb,
    ssize_t size,
    int (*compar)(const void *, const void *, void *),
    void *user,
    ssize_t *p_low,
    ssize_t *p_high)
{
    // If something is missign set size to 0, so we fall through to exit.
    if (!key || !base || !compar) { nmemb = 0; }
    const char *cbase = base;
    int cmp = 1;
    size_t mid = 0;
    size_t lo = 0;
    size_t hi = nmemb;
    while (lo < hi)
    {
        // This is just `mid = (hi + lo)/2;` but without overflow on (hi + lo).
        mid = lo + (hi - lo)/2;
        cmp = compar(key, cbase + size*mid, user);
        if (cmp < 0) { hi = mid; }
        if (0 < cmp) { lo = mid + 1; }
        if (!cmp) {
            // Found a mathching element.
            lo = hi = mid;
            // Search for earliest matching neighbour.
            while (0 < lo && !compar(key, cbase + size*(lo-1), user)) { lo--; }
            // Search for last matching neighbour.
            while (hi < nmemb && !compar(key, cbase + size*hi, user)) { hi++; }
            // Set cmp return value and exit.
            cmp = 0;
            break;
        }
    }
    // Exit:
    // If no element found, we exited the while with (lo == hi).
    // If elements found, we `break;`ed while and the values of
    //      lo and hi are set to offsets, needed by p_low and p_high.
    if(p_low) { *p_low = lo; } 
    if(p_high) { *p_high = hi; } 
    // Similarly, either a fallthorugh or normal while exit sets !cmp == 0
    //      and `break;`ing sets !cmp == 1.
    return !cmp;
}
