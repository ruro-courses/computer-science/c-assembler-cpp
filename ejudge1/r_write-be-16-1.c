#include <sys/types.h>
#include <sys/stat.h>
#include <endian.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    int fd_out = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
    unsigned short temp;
    while(1 == scanf("%hu", &temp)) {
        temp = htobe16(temp);
        write(fd_out, &temp, sizeof(temp));
    }
    return 0;
}
