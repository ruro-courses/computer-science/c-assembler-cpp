#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

int run(const char *cmd)
{
    int wstatus;
    pid_t child = fork();
    if (child == -1) {
        return 0; // False
    }
    if (!child) {
        execlp(cmd, cmd, NULL);
        exit(-1); // False
    }
    while (waitpid(child, &wstatus, 0) != -1 && errno != ECHILD) {
        if (WIFEXITED(wstatus)) {
            return !WEXITSTATUS(wstatus); // Status
        }
        errno = 0;
    }
    return 0; // False
}

int main(int argc, char *argv[])
{
    return !((run(argv[1]) || run(argv[2])) && run(argv[3]));
}
