#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>

volatile sig_atomic_t counter = 0;
volatile intmax_t last_prime = 0;

enum { MAX_COUNTER = 3 };

void handl(int sig)
{
    sig_atomic_t local = counter++;
    if (local >= MAX_COUNTER || sig == SIGTERM) {
        exit(0);
    }
    printf("%ju\n", last_prime);
    fflush(stdout);
}

bool isPrime(intmax_t a) {
    intmax_t divisor = 3;
    intmax_t composite = 0;
    while (true) {
        composite = a / divisor;
        if (composite < divisor) {
            return true;
        }
        if (a == divisor * composite) {
            return false;
        }
        divisor += 2;
    }
}

int main(void)
{
    sigset_t set;
    sigfillset(&set);
    struct sigaction act;
    act.sa_handler = handl;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    printf("%ju\n", (uintmax_t)getpid());
    fflush(stdout);

    intmax_t low, high;
    scanf("%jd %jd", &low, &high);
    low = (low < 0) ? 0 : low;
    low += !(low % 2);
    high -= !(high % 2);
    for (intmax_t i = low; i <= high; i += 2) {
        if (isPrime(i)) {
            last_prime = i;
        }
    }
    printf("-1\n");
    fflush(stdout);
    return 0;
}
