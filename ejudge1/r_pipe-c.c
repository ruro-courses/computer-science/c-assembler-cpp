#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/wait.h>

typedef struct
{
    int32_t val;
    bool ok;
} Packet;

void child(int fd[2])
{
    if (!fork())
    {
        // Child.
        close(fd[1]);
        int64_t sum = 0;
        Packet pak;
        pak.ok = true;
        pak.val = 0;
        while (pak.ok) {
            sum += pak.val;
            while (read(fd[0], &pak, sizeof(pak)) != sizeof(pak));
        }
        printf("%"PRId64"\n", sum);
        fflush(stdout);
        exit(0);
    }
    close(fd[0]);
}

int main(void)
{
    int fd[2];
    pipe(fd);
    child(fd);
    Packet pak;
    pak.ok = true;
    while (scanf("%"SCNd32, &pak.val) == 1) {
        while (write(fd[1], &pak, sizeof(pak)) != sizeof(pak));
    }
    pak.ok = false;
    while (write(fd[1], &pak, sizeof(pak)) != sizeof(pak));
    while (wait(NULL) != -1 || errno != ECHILD);
}
