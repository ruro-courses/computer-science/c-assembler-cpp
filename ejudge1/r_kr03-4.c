#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

int waitcount(void)
{
    int wstatus;
    while (wait(&wstatus) != -1 && errno != ECHILD) {
        if (WIFEXITED(wstatus)) {
            return !WEXITSTATUS(wstatus);
        }
        if (WIFSIGNALED(wstatus)) {
            return 0;
        }
        errno = 0;
    }
    return 0;
}

int run(const char *cmd, bool towait)
{
    pid_t child = fork();
    if (child == -1) {
        return 0;
    }
    if (!child) {
        execlp(cmd, cmd, NULL);
        exit(-1);
    }
    if (towait)
        return waitcount();
    return 0;
}

int main(int argc, char *argv[])
{
    size_t counter = 0;
    char **start = &argv[3];
    int s1 = 0, s2 = 0;
    while ((s1 = run(argv[1], true)) || (s2 = run(argv[2], true))) {
        counter += s1;
        counter += s2;
        s1 = s2 = 0;
        for (size_t i = 0; start[i]; i++) {
            run(start[i], false);
        }
        for (size_t i = 0; start[i]; i++) {
            counter += waitcount();
        }
    }
    printf("%zu\n", counter);
    return 0;
}
