#include <stdio.h>

int main(void)
{
    size_t write_zone = 0;
    size_t exec_zone = 0;
    size_t start, end;
    char w, x;
    while (scanf("%x-%x %*c%c%c %*[^\n]\n", &start, &end, &w, &x) == 4)
    {
        if (w == 'w') {
            write_zone += end - start;
        }
        if (x == 'x') {
            exec_zone += end - start;
        }
    }
    printf("%zu\n%zu\n", write_zone, exec_zone);
}
