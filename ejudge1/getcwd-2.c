#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

char *getcwd_unlocked(void)
{
    struct stat c_stat, p_stat;
    if (lstat(".", &c_stat) || lstat("..", &p_stat)) {
        return NULL;
    }
    char *p_name = NULL;
    char *c_name = NULL;
    char *tmp = NULL;
    if (c_stat.st_dev == p_stat.st_dev && c_stat.st_ino == p_stat.st_ino) {
        // We are currently at root, return "/"
        p_name = malloc(sizeof("/"));
        if (!p_name) {
            return NULL;
        }
        strncpy(p_name, "/", sizeof("/"));
        return p_name;
    }
    if (chdir("..")) {
        return NULL;
    }
    DIR *p_dir = opendir(".");
    if (!p_dir) {
        return NULL;
    }
    struct dirent *file = NULL;
    while ((file = readdir(p_dir))) {
        if (lstat(file->d_name, &p_stat)) {
            return NULL;
        }
        // If inode number matches, we have found our current directory.
        if (c_stat.st_dev == p_stat.st_dev && c_stat.st_ino == p_stat.st_ino) {
            // Save the current dir name.
            size_t c_len = strlen(file->d_name) + 1;
            tmp = realloc(c_name, c_len);
            if (!tmp) {
                free(c_name);
                return NULL;
            }
            c_name = tmp;
            strncpy(c_name, file->d_name, c_len);
            closedir(p_dir);

            // Get the parent dir name.
            p_name = getcwd_unlocked();
            if (p_name) {
                // Append path.
                size_t p_len = strlen(p_name) + 1;
                tmp = realloc(p_name, p_len + c_len);
                if (!tmp) {
                    free(p_name);
                    p_name = NULL;
                } else {
                    p_name = tmp;
                    strncat(p_name, c_name, c_len - 1);
                    strncat(p_name, "/", 1);
                }
            }
            // chdir back.
            if (chdir(c_name)) {
                free(p_name);
                free(c_name);
                return NULL;
            }
            free(c_name);
            return p_name;
        }
    }
    // Directory not found in its parent dir, error.
    free(p_name);
    free(c_name);
    return NULL;
}

ssize_t getcwd2(int fd, char *buf, size_t size)
{
    // Read real cwd, to restore it later.
    char *oldcwd = NULL;
    char *fdcwd = NULL;
    size_t len = -1;
    if (!(oldcwd = getcwd_unlocked())) {
        return -1;
    }

    // Get fd dir.
    if (!fchdir(fd) && (fdcwd = getcwd_unlocked())) {
        len = strlen(fdcwd);
        if (size) {
            strncpy(buf, fdcwd, size);
            buf[size - 1] = '\0';
            if (len < size && len > 1) {
                buf[len - 1] = '\0';
            }
        }
    }

    // Return, clean up.
    if (chdir(oldcwd)) {
        len = -1;
    }
    free(oldcwd);
    free(fdcwd);
    return len;
}
