#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int voidcasecmp(const void *a, const void *b)
{
    return strcasecmp(*(const char **)a,*(const char **)b);
}

size_t getsubdirs(char *dirname, char ***list)
{
    DIR *dir = opendir(dirname);
    if (!dir) {
        return 0;
    }
    struct dirent *file;
    struct stat filestat;
    char buf[PATH_MAX];
    size_t dirnum = 0;
    size_t namelen = 0;

    while ((file = readdir(dir))) {
        if (!strcmp(file->d_name, ".") || !strcmp(file->d_name, "..")) {
            continue;
        }
        snprintf(buf, sizeof(buf), "%s/%s", dirname, file->d_name);
        lstat(buf, &filestat);
        if (S_ISDIR(filestat.st_mode)) {
            *list = realloc(*list, ++dirnum * sizeof(char*));
            namelen = strlen(file->d_name) + 1;
            (*list)[dirnum-1] = malloc(namelen);
            snprintf((*list)[dirnum-1], namelen, "%s", file->d_name);
        }
    } 
    qsort(*list, dirnum, sizeof(char*), voidcasecmp);
    closedir(dir);
    return dirnum;
}

int testdir(char *dirname)
{
    DIR *dir = opendir(dirname);
    if (!dir) {
        return 0;
    }
    closedir(dir);
    return 1;
}

void traverse(char *dirname)
{
    char **dirs = NULL;
    char buf[PATH_MAX];
    size_t num = getsubdirs(dirname, &dirs);
    for (size_t i = 0; i < num; i++) {
        snprintf(buf, sizeof(buf), "%s/%s", dirname, dirs[i]);
        if (!testdir(buf)) {
            free(dirs[i]);
            continue;
        }
        printf("cd %s\n", dirs[i]);
        traverse(buf);
        free(dirs[i]);
        printf("cd ..\n");
    }
    free(dirs);
}

int main(int argc, char *argv[])
{
    traverse(argv[1]);
}
