#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void)
{
    // Parent
    if (!fork())
    {
        // Child
        if (!fork())
        {
            // Grandchild
            printf("3 ");
            exit(0);
        }
        // Wait for grandchild to stoop
        while (wait(NULL) != -1);
        printf("2 ");
        exit(0);
    }
    // Wait for child to stoop
    while (wait(NULL) != -1);
    printf("1\n");
    exit(0);
}
