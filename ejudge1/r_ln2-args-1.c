#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <errno.h>

enum { STRTO_BASE = 10 };

int main(int argc, char *argv[])
{
    char *endptr;
    while (*++argv) {
        // Read an unsigned long.
        errno = 0;
        unsigned long temp = strtoul(*argv, &endptr, STRTO_BASE);

        // Either the string was empty,
        // found invalid character before EOL
        // or number can't be represented as uint32_t.
        if (**argv == '\0' || *endptr != '\0' || temp > UINT32_MAX || errno == ERANGE) {
            printf("-1\n");
            continue;
        }

        // (uint32_t)(unsigned long)x is equivalent to (uint32_t)x for values
        // that can be represented and yields the same results on both 32 bit
        // and 64 bit platforms.
        uint32_t val = temp;

        // __builtin_clz is undefined, when val == 0
        if (val == 0) {
            printf("0\n");
            continue;
        }

        // __builtin_clz returns the number of leading 0 bits,
        // so (UINT32_BITS - clz) is the position of the most significant bit.
        // Conviniently, this is also the value of ceil(log2(val + 1));
        printf("%zu\n", (sizeof(uint32_t) * CHAR_BIT) - __builtin_clz(val));
    }
}
