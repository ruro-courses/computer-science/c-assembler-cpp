#define _XOPEN_SOURCE
#include <stdio.h>
#include <time.h>
#include <string.h>

enum { BUF_SIZE = 1001 };

time_t parse_time(char *str)
{
    struct tm tstamp;
    strptime(str, "%Y/%m/%d %H:%M:%S", &tstamp);
    tstamp.tm_isdst = -1;
    return mktime(&tstamp);
}

int main(int argc, char *argv[])
{
    FILE *file;
    char buf[BUF_SIZE];
    time_t prev, next;
    file = fopen(argv[1], "r");
    fgets(buf, sizeof(buf), file);
    prev = parse_time(buf);
    while (fgets(buf, sizeof(buf), file)) {
        next = parse_time(buf);
        printf("%ld\n", next - prev);
        prev = next; 
    }
}
