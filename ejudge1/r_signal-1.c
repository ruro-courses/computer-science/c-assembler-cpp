#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <inttypes.h>
#include <sys/types.h>

volatile sig_atomic_t mode = SIGUSR1;

void modechange(int sig)
{
    mode = (sig_atomic_t)sig;
}

int main(void)
{
    sigset_t set;
    sigfillset(&set);
    struct sigaction act;
    act.sa_handler = modechange;
    act.sa_mask = set;
    act.sa_flags = 0;
    sigaction(SIGUSR1, &act, NULL);
    sigaction(SIGUSR2, &act, NULL);
    printf("%ju\n", (uintmax_t)getpid());
    fflush(stdout);
    intmax_t num;
    while (1) {
        errno = 0;
        if (scanf("%jd", &num) != 1) {
            if (errno != EINTR) {
                return 0;
            }
            continue;
        }
        switch (mode) {
            case SIGUSR1:
                num *= -1;
                break;
            case SIGUSR2:
                num *= num;
                break;
            default:
                break;
        }
        printf("%jd\n", num);
        fflush(stdout);
    }
}
