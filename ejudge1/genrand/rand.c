#include "rand.h"

#include <stdlib.h>

int rand_range(int low, int high)
{
    return low + (int)((high - low) * (double)rand() / ((double)RAND_MAX + 1));
}

