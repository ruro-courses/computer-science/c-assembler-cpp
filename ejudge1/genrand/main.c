#include "rand.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int count, low, high;
    sscanf(argv[1], "%d", &count);
    sscanf(argv[2], "%d", &low);
    sscanf(argv[3], "%d", &high);

    unsigned seed;
    sscanf(argv[4], "%u", &seed);
    srand(seed);
    
    while (count--)
    {
        printf("%d\n", rand_range(low, high));
    }
}
