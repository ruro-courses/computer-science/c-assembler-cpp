#include <stdio.h>

int main(int argc, char *argv[])
{
    double inc, dec, temp, account;
    sscanf(*++argv, "%lf", &account);
    sscanf(*++argv, "%lf", &inc);
    sscanf(*++argv, "%lf", &dec);
    inc /= 100;
    dec /= 100;
    inc += 1;
    dec += 1;
    while (*++argv) {
        sscanf(*argv, "%lf", &temp);
        account += temp;
        account *= (account > 0) ? inc : dec ;
    }
    printf("%.10g\n", account);
    return 0;
}
