#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdio.h>

int main(void)
{
    wint_t c;
    size_t num = 0;
    size_t upp = 0;
    size_t low = 0;
    setlocale(LC_ALL, "");
    while ((c = getwc(stdin)) != WEOF) {
        if (iswdigit(c)) {
            num++;
        }
        if (iswupper(c)) {
            upp++;
        }
        if (iswlower(c)) {
            low++;
        }
    }
    printf("%zu\n%zu\n%zu\n", num, upp, low);
}
