#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>

off_t filesize(const char *dirname, const char *filename)
{
    // Get rid of extra '/' in dirname.
    char sep[] = "/";
    if (dirname[strlen(dirname) - 1] == '/') {
        sep[0] = '\0';
    }

    // Get the lenght of {dirname}/{filename}.
    size_t pathlen = 1 + snprintf(NULL, 0, "%s%s%s", dirname, sep, filename);
    pathlen = (pathlen > PATH_MAX) ? PATH_MAX : pathlen;

    // Obtain a string with full filename.
    char filepath[pathlen];
    snprintf(filepath, pathlen, "%s%s%s", dirname, sep, filename);

    // Get stat of flie.
    struct stat filestat;
    stat(filepath, &filestat);

    // Only files we own.
    // Only regular files.
    // Only files with filename starting with a capital letter. 
    if (filestat.st_uid == getuid()
            && S_ISREG(filestat.st_mode)
            && isupper(filename[0])) {
        return filestat.st_size;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    DIR *dir = opendir(argv[1]);
    struct dirent *file;
    long long unsigned size = 0;
    while ((file = readdir(dir))) {
        size += filesize(argv[1], file->d_name);
    }
    printf("%llu\n", size);
    closedir(dir);
}
