#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

volatile sig_atomic_t counter = 0;

void run(const char *str)
{
    pid_t child = fork();
    if (!child) {
        execlp(str, str, NULL);
        exit(-1);
    }
}

void waitcount(void)
{
    int wstatus;
    while (wait(&wstatus) != -1) {
        counter += WIFEXITED(wstatus) && !WEXITSTATUS(wstatus);
    }
}

char *getcmd(char *filename)
{
    FILE *f = fopen(filename, "r");
    char *line = NULL;
    size_t n = 0;
    getline(&line, &n, f);
    fclose(f);
    line[strlen(line) - 1] = '\0';
    return line;
}

int main(int argc, char *argv[])
{
    int N;
    argv++;
    sscanf(*argv, "%d", &N);
    argv++;
    for (int i = 0; *argv && i < N; i++, argv++) {
        char *cmd = getcmd(*argv);
        run(cmd);
        free(cmd);
    }
    waitcount();
    for (; *argv; argv++) {
        char *cmd = getcmd(*argv);
        run(cmd);
        waitcount();
        free(cmd);
    }
    printf("%ju\n", (intmax_t)counter);
}
