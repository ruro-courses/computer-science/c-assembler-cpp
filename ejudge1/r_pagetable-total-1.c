#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>

enum { ROOT_OFF = 22 };
enum { TABLE_SIZE = 1024 };
enum { ELEMENT_SIZE = 4 };

int main(void)
{
    uint32_t start, finish;
    size_t count = 0;
    bool pages[TABLE_SIZE] = { 0 };
    while (scanf("%x-%x%*[^\n]\n", &start, &finish) == 2) {
        count += !count;
        finish--;
        start >>= ROOT_OFF;
        finish >>= ROOT_OFF;
        while (start <= finish) {
            count += !pages[start];
            pages[start] |= 1;
            start++;
        }
    }
    printf("%zu\n", count*TABLE_SIZE*ELEMENT_SIZE);
}
