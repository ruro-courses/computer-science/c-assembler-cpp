#include <sys/types.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

enum { STRTO_BASE = 10 };

uint64_t total_size = 0;
char *path = NULL;
char *suffix = NULL;

int issuffixed(const char *a)
{
    size_t len = strlen(a);
    size_t end = strlen(suffix);
    if (end > len)
        return 0;
    return 0 == strncmp(a + len - end, suffix, end);
}

void getstat(const char *name, struct stat *target)
{
    char fullname[PATH_MAX];
    snprintf(fullname, PATH_MAX, "%s/%s", path, name);
    lstat(fullname, target);
}

int dirfilter(const struct dirent *dir)
{
    struct stat filestat;
    getstat(dir->d_name, &filestat);
    if (!S_ISREG(filestat.st_mode)) {
        return 0;
    }
    total_size += filestat.st_size;
    return issuffixed(dir->d_name);
}

int dircmp(const struct dirent **dirA, const struct dirent **dirB)
{
    struct stat filestatA, filestatB;
    getstat((*dirA)->d_name, &filestatA);
    getstat((*dirB)->d_name, &filestatB);
    int64_t ret = filestatA.st_size - filestatB.st_size;
    if (ret)
        return (ret < 0) ? 1 : -1;
    return strcmp((*dirA)->d_name, (*dirB)->d_name);
}

int main(int argc, char *argv[])
{
    uint64_t max_size = strtoull(argv[2], NULL, STRTO_BASE);
    path = argv[1];
    suffix = argv[3];

    struct dirent **namelist = NULL;
    scandir(path, &namelist, dirfilter, dircmp);
    if (total_size > max_size && namelist && namelist[0]) {
        printf("%s\n", namelist[0]->d_name);
    }
}
