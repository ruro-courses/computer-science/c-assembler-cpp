#include <stdlib.h>
#include <stdio.h>

typedef struct
{
    int val;
    int prob;
} RandVal;

int randf_pcent(void)
{
    double res = rand();
    res /= (double)RAND_MAX + 1;
    res *= 100;
    return res;
}

int main(void)
{
    int num;
    scanf("%d", &num);

    RandVal choice[100];
    for (int i = 0; i < num; i++) {
        scanf("%d %d", &choice[i].val, &choice[i].prob);
    }

    int req;
    scanf("%d", &req);

    unsigned seed;
    scanf("%u", &seed);
    srand(seed);

    while (req--) {
        int pcent = randf_pcent();
        int count = 0;
        while ((pcent -= choice[count].prob) >= 0) {
            count++;
        }
        printf("%d\n", choice[count].val);
    }
}
