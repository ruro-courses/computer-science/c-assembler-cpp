#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <endian.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

typedef struct
{
    int16_t x;
    int64_t y;
} Data;

void stop(int, const char*) __attribute__((noreturn));
void bread(int, void*, size_t);
void bwrite(int, const void*, size_t);
void writeData(int, size_t, Data*);
void readData(int, size_t, Data*);
void processData(int32_t, Data*);
size_t Datalen(int);

void stop(int code, const char *message)
{
    fprintf(stderr, "%s\n", message);
    if (errno) {
        fprintf(stderr, "Error: %s\n", strerror(errno));
    }
    exit(code);
}

void bread(int fd, void *dst, size_t count)
{
    char *buf = dst;
    ssize_t temp = 0;
    while (count) {
        temp = read(fd, buf, count);
        if (temp < 0) {
            if (errno != EINTR) {
                stop(2, "Unable to read.");
            }
            continue;
        }
        count -= temp;
        buf   += temp;
    }
}

void bwrite(int fd, const void *src, size_t count)
{
    const char *buf = src;
    ssize_t temp = 0;
    while (count) {
        temp = write(fd, buf, count);
        if (temp < 0) {
            if (errno != EINTR) {
                stop(2, "Unable to write.");
            }
            continue;
        }
        count -= temp;
        buf   += temp;
    }
}

void readData(int fd, size_t pos, Data *buf)
{
    if (lseek(fd, pos * (sizeof(buf->x) + sizeof(buf->y)), SEEK_SET) < 0) {
        stop(2, "Unable to lseek.");
    }
    bread(fd, &buf->x, sizeof(buf->x));
    bread(fd, &buf->y, sizeof(buf->y));
    buf->x = le16toh(buf->x);
    buf->y = le64toh(buf->y);
}

void writeData(int fd, size_t pos, Data *buf)
{
    if (lseek(fd, pos * (sizeof(buf->x) + sizeof(buf->y)), SEEK_SET) < 0) {
        stop(2, "Unable to lseek.");
    }
    buf->x = htole16(buf->x);
    buf->y = htole64(buf->y);
    bwrite(fd, &buf->x, sizeof(buf->x));
    bwrite(fd, &buf->y, sizeof(buf->y));
}

void processData(int32_t A, Data *d)
{
    // This never overflows, because int16_t * int32_t fits into int64_t.
    int64_t tmp = (int64_t)d->x * (int64_t)A;
    // Check overflow on addition.
    if (__builtin_add_overflow(tmp, d->y, &d->y)) {
        errno = EOVERFLOW;
        stop(3, "Addition overflowed.");
    }
}

size_t Datalen(int fd)
{
    if (lseek(fd, 0, SEEK_SET) < 0) {
        stop(2, "Unable to lseek.");
    }
    off_t ret;
    if ((ret = lseek(fd, 0, SEEK_END)) < 0) {
        stop(2, "Unable to lseek.");
    }
    size_t datalen = sizeof(((Data*)0)->x) + sizeof(((Data*)0)->y);
    if (ret % datalen) {
        stop(2, "The length of file is not a multiple of Data.");
    }
    return ret / datalen;
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        stop(2, "Wrong number of arguments.");
    }
    int fd;
    if ((fd = open(argv[1], O_RDWR)) < 0 ) {
        stop(2, "Can't open file.");
    }
    int32_t A = strtol(argv[2], NULL, 10);
    size_t len = Datalen(fd);
    Data data1, data2;
    for (size_t i = 0; i < (len+1)/2; i++) {
        readData(fd, i, &data1);
        readData(fd, len-i-1, &data2);
        processData(A, &data1);
        processData(A, &data2);
        writeData(fd, i, &data2);
        writeData(fd, len-i-1, &data1);
    }
}
