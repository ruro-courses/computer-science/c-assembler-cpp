#include <stdio.h>
#include <inttypes.h>

int main(void)
{
    int32_t bits, width;
    // SCNd32 is just the scanf spec for int32_t
    scanf("%"SCNd32" %"SCNd32, &bits, &width);
    int32_t max_intn = (1 << (bits - 1)) - 1;
    int32_t min_intn = ~max_intn;
    int32_t i, j;
    for (i = 0; i <= max_intn; i++) {
        printf("|%1$*2$"PRIu32"|%1$*2$zx|%1$*2$"PRId32"|\n", i, width);
    }
    for (j = min_intn; j < 0; j++, i++) {
        printf("|%2$*3$"PRIu32"|%2$*3$zx|%1$*3$"PRId32"|\n", j, i, width);
    }
}
