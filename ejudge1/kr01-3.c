char *strend(char *a)
{
    if (!a || !*a) {
        return a;
    }
    while (*a) {
        a++;
    }
    return a - 1;
}

char *nextdir(char *a, char *end)
{
    if (a < end) {
        a++;
    }
    while (a < end && *a != '/') {
        a++;
    }
    return a;
}

char *prevdir(char *a, char *start)
{
    if (a != start) {
        a--;
    }
    while (a > start && *a != '/') {
        a--;
    }
    return a;
}

void strshift(char *src, char *dest)
{
    while (*src) {
        *dest = *src;
        dest++;
        src++;
    }
    *dest = *src;
}

void normalize_path(char *buf)
{
    char *dir = buf;
    while (dir < strend(buf)) {
        if (dir[1] == '.' && dir[2] == '.' && (dir[3] == '/' || dir[3] == '\0')) {
            char *rest = &dir[3];
            dir = prevdir(dir, buf);
            strshift(rest, dir);
            // cd ..
        } else if (dir[1] == '.' && (dir[2] == '/' || dir[2] == '\0')) {
            char *rest = &dir[2];
            strshift(rest, dir); 
            // cd .
        } else {
            dir = nextdir(dir, strend(buf));
        }
    }
    if (dir != buf && dir[0] == '/') {
        dir[0] = '\0'; 
    }
    if (dir == buf) {
        dir[0] = '/';
        dir[1] = '\0';
    }
}
