#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>

int run(const char *cmd)
{
    int wstatus;
    pid_t child = fork();
    if (child == -1) {
        return 0; // False
    }
    if (!child) {
        execlp(cmd, cmd, NULL);
        exit(-1); // False
    }
    while (waitpid(child, &wstatus, 0) != -1 && errno != ECHILD) {
        if (WIFEXITED(wstatus)) {
            return !WEXITSTATUS(wstatus); // Status
        }
        errno = 0;
    }
    return 0; // False
}

enum
{
    S_IRWU = S_IRUSR | S_IWUSR,
    S_IRWG = S_IRGRP | S_IWGRP,
    S_IRWO = S_IROTH | S_IWOTH,
    S_IRWA = S_IRWU | S_IRWG | S_IRWO
};

int main(int argc, char *argv[])
{
    int fd[2];
    pid_t chld;
    pipe(fd);
    // (
    if (!(chld = fork()))
    {
        // |
        dup2(fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        // < file1
        int STDIN_OLD = dup(STDIN_FILENO);
        int file1 = open(argv[4], O_RDONLY);
        dup2(file1, STDIN_FILENO);
        close(file1);
        // cmd1
        if (run(argv[1])) {
            // &&
            // #undoing < file1 for cmd2
            dup2(STDIN_OLD, STDIN_FILENO);
            // cmd2
            run(argv[2]);
        }
        close(STDOUT_FILENO);
        exit(0);
    }
    // )
    // |
    dup2(fd[0], STDIN_FILENO);
    close(fd[1]);
    close(fd[0]);
    // >> file2
    int file2 = open(argv[5], O_WRONLY | O_CREAT | O_APPEND, S_IRWA);
    dup2(file2, STDOUT_FILENO);
    close(file2);
    // cmd3
    run(argv[3]);
    close(STDIN_FILENO);
    // ;
    while (waitpid(chld, NULL, 0) != -1 && errno != ECHILD) {
        errno = 0;
    }
}
