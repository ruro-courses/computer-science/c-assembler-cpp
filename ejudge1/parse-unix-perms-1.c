#include <string.h>

int parse_rwx_permissions(const char *str)
{
    const char access[] = "rwxrwxrwx";
    const int acc_len = sizeof(access) - 1;
    if (!str || strlen(str) != acc_len) { return -1; }
    int rights = 0;
    for (int i = 0; i < acc_len; i++) {
        rights <<= 1;
        if (str[i] == access[i]) { rights++; }
        else if (str[i] != '-') { return -1; }
    }
    return rights;
}
