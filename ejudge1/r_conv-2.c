// Includes are here only for types and macros.
// Implementations are 100% local.
#include <syscall.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

// Disable canary stack checks.
// We could also implement __stack_chk_fail_local(void).
#pragma GCC optimize ("-fno-stack-protector")

// Forward declaration for my functions.
void printu32(uint32_t);
uint32_t scanu32(void);
void putch(char);
char getch(void);
int scall(int, int, int, int);

void _start(void)
{
    uint32_t N = scanu32();
    if (N < 2) {
        exit(EXIT_SUCCESS);
    }
    size_t avail = 1;
    uint32_t *buf = sbrk(0);
    sbrk(sizeof(*buf));
    for (int i = 0; i < N; i++) {
        if (i >= avail) {
            if (2*avail <= N) {
                sbrk(avail*sizeof(*buf));
                avail *= 2;
            } else {
                sbrk((N - avail)*sizeof(*buf));
                avail = N;
            }
        }
        buf[i] = scanu32();
    }
    for (int i = 0; i < N-1; i++) {
        printu32(buf[i] + buf[i+1]);
        putch('\n');
    }
    putch('\0');
    exit(EXIT_SUCCESS);
}

int scall(int id, int arg1, int arg2, int arg3)
{
    // Syscall wrapper.
    // See Linux x86 conventions.
    register int eax asm("eax") = id;
    register int ebx asm("ebx") = arg1;
    register int ecx asm("ecx") = arg2;
    register int edx asm("edx") = arg3;
    // Call the kernel.
    asm volatile("int $0x80" : "+r" (eax) : "r" (ebx), "r" (ecx), "r" (edx));
    // Forward the return.
    return eax;
}

// WARNING!
// The functions implemented below have prototypes of stdlib wrappers, but the
// behaviour differs. Do not use them if you don't understand how they work.

void exit(int val)
{
    // Exit normally.
    scall(__NR_exit , val, 0, 0);
    // Halt and catch fire.
    asm volatile("hlt");
    // Catching fire failed, just give up.
    __builtin_unreachable();
}

ssize_t read(int fd, void *buf, size_t count)
{
    // Tries to read a block of `count` bytes. Returns the number of bytes
    // actually read.
    ssize_t temp = 0;
    do {
        temp = scall(__NR_read, fd, (int)buf, count);
    } while (temp < 0);
    return temp;
}

ssize_t write(int fd, const void *buf, size_t count)
{
    // Tries to write a block of `count` bytes. Returns the number of bytes
    // actually written.
    ssize_t temp = 0;
    const char *run_p = buf;
    do {
        temp = scall(__NR_write, fd, (int)run_p, count);
        if (temp < 0) {
            continue;
        }
        count -= temp;
        run_p += temp;
    } while (count);
    return run_p - (char *)buf;
}

void *sbrk(intptr_t size)
{
    // Moves the program break by `size` bytes. Returns the previous program
    // break. Thus it effectively works as malloc(size), when no errors are
    // encountered. Note: memory cannot be deallocated.
    void *brkp = (void *)scall(__NR_brk, 0, 0, 0);
    scall(__NR_brk, (int)brkp + size, 0, 0);
    return brkp;
}

enum { BUF_SIZE = 255 };

char getch(void)
{
    // Get the next charracter from the buffered input.
    // If the buffer is empty, fetch from stdin.
    static char buf[BUF_SIZE];
    static int pos = -1;
    static ssize_t avail = 0;
    pos += 1;
    if (pos == BUF_SIZE || pos >= avail) {
        avail = read(STDIN_FILENO, buf, sizeof(buf));
        pos = 0;
        if (!avail) {
            return '\0';
        }
    }
    pos %= BUF_SIZE;
    return buf[pos];
}

void putch(char c)
{
    // Put the character c into buffered output.
    // If char is '\0' or the buffer is full, flush the output.
    static char buf[BUF_SIZE];
    static int pos = -1;
    pos += 1;
    if (pos == BUF_SIZE || c == '\0') {
        write(STDOUT_FILENO, buf, pos);
        if (c == '\0') {
            pos = 0;
            return;
        }
    }
    pos %= BUF_SIZE;
    buf[pos] = c;
}

int isdigit(int c)
{
    return (('0' <= c) && (c <= '9'));
}

enum { NUM_BASE = 10 };

uint32_t scanu32(void)
{
    // Scan an unsigned number from stdin. Return it's value in uint32_t.
    // Skip all leading non-digits. Consume 1 extra char at the end.
    char c = '0';
    uint32_t acc = 0;
    while ((c = getch()) && !isdigit(c));

    do {
        acc *= NUM_BASE;
        acc += c - '0';
    } while ((c = getch()) && isdigit(c));

    return acc;
}

void printu32(uint32_t num)
{
    // Print an unsigned number to stdout.
    if (num / NUM_BASE) {
        printu32(num / NUM_BASE);
    }
    putch('0' + (num % NUM_BASE));
}
