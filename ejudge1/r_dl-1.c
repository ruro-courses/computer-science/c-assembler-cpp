#include <dlfcn.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    double (*fptr)(double);
    *((void **)&fptr) = dlsym(dlopen("/lib/libm.so.6", RTLD_LAZY), argv[1]);
    double temp;
    while (scanf("%lf", &temp) == 1) {
        printf("%.10g\n", fptr(temp));
    }
}
