#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

enum { TERMSIG_SH = 128 };

int mysys(const char *str)
{
    pid_t child = fork();
    if (child == -1) {
        return -1;
    }
    if (!child) {
        execl("/bin/sh", "sh", "-c", str, NULL);
        // Nothing portable we can do?
        exit(-1);
    }
    int wstatus;
    while (1) {
        waitpid(child, &wstatus, 0);
        if (WIFEXITED(wstatus)) {
            return WEXITSTATUS(wstatus);
        }
        if (WIFSIGNALED(wstatus)) {
            return WTERMSIG(wstatus) + TERMSIG_SH;
        }
    }
}
