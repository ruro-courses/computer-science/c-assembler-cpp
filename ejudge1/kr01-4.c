#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

enum { MAX_SIZE = USHRT_MAX + 1 };

size_t bread(int fd, void *target, size_t count)
{
    ssize_t temp = 0;
    size_t done = 0;
    char *buf = target;
    while (count) {
        temp = read(fd, buf, count);
        if (temp <= 0) {
            if (errno != EINTR) {
                exit(-errno);
            }
            errno = 0;
            continue;
        }
        count -= temp;
        done  += temp;
        buf   += temp;
    }
    return done;
}

int getNextObj(int fd, char *object)
{
    static off_t pos = 0;
    unsigned short len;
    int offset;
    bread(fd, &len, sizeof(len));
    bread(fd, &offset, sizeof(offset));
    bread(fd, object, len);
    object[len] = '\0';

    if (!offset) {
        return 0;
    }
    
    pos += offset;
    lseek(fd, pos, SEEK_SET);
    return 1;
}

int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDONLY);
    if (!fd) {
        return 0;
    }
    
    char next[MAX_SIZE] = "";
    char max[MAX_SIZE] = "";

    while (getNextObj(fd, next)) {
        if (strcmp(next, max) > 0) {
            strcpy(max, next);
        }
    }

    if (strcmp(next, max) > 0) {
        strcpy(max, next);
    }
    printf("%s\n", max);
    close(fd);
}
