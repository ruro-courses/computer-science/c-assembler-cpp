int
myisspace(char c)
{
    if (c == ' '  ||
        c == '\t' ||
        c == '\n' ||
        c == '\r' ||
        c == '\v' ||
        c == '\f')
        return 1;
    else
        return 0;
}

int
mystrspccmp(const char *a, const char *b)
{
    while (*a || *b) {
        if (myisspace(*a)) {
            a++;
            continue;
        }
        if (myisspace(*b)) {
            b++;
            continue;
        }
        if (*a != *b)
            return *(unsigned char *)a - *(unsigned char *)b;
        a++;
        b++;
    }
    return 0;
}
