#include <sys/wait.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <inttypes.h>

intmax_t max = 0;

FILE *pipe_out = NULL;
FILE *pipe_inp = NULL;

sigset_t usr1_set;

void send(pid_t from, pid_t to, intmax_t message)
{
    fprintf(pipe_inp, "%jd %jd|", (intmax_t)from, message);
    fflush(pipe_inp);
    kill(to, SIGUSR1);
}

intmax_t recieve(pid_t *from)
{
    intmax_t rec, message;
    int sig;
    sigwait(&usr1_set, &sig);
    fscanf(pipe_out, "%jd %jd|", &rec, &message);
    *from = rec;
    return message;
}

pid_t child(intmax_t id)
{
    pid_t self;
    pid_t friend;
    if (!(self = fork()))
    {
        self = getpid();
        intmax_t count;
        while (true) {
            count = recieve(&friend);
            count++;
            if (count >= max || count <= 0)
            {
                send(self, friend, -1);
                break;
            }
            printf("%jd %jd\n", id, count);
            fflush(stdout);
            send(self, friend, count);
        }
        exit(0);
    }
    return self;
}

int main(int argc, char *argv[])
{
    // Make a pipe FILE*
    int fd[2];
    pipe(fd);
    pipe_out = fdopen(fd[0], "r");
    pipe_inp = fdopen(fd[1], "w");

    // Block SIGUSR1
    sigemptyset(&usr1_set);
    sigaddset(&usr1_set, SIGUSR1);
    sigprocmask(SIG_BLOCK, &usr1_set, NULL);

    sscanf(argv[1], "%jd", &max);

    pid_t ch1 = child(1);
    pid_t ch2 = child(2);

    send(ch2, ch1, 0);

    fclose(pipe_out);
    fclose(pipe_inp);

    while (wait(NULL) != -1 || errno != ECHILD); 

    printf("Done\n");
    fflush(stdout);
}
