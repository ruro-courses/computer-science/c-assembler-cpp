#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void child(int id)
{
    if (fork())
    {
        return;
    }
    
    char buf[9];
    read(STDIN_FILENO, buf, sizeof(buf)-1);
    buf[8] = '\0';

    int32_t tmp = 0;
    sscanf(buf, "%d", &tmp);
    tmp *= tmp;
    printf("%d %d\n", id, tmp);
    exit(0);
}

int main(void)
{
    child(1);
    child(2);
    child(3);
    while (wait(NULL) != -1);
}
