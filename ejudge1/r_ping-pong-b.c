#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

volatile int *cnt = NULL;
volatile int *ready = NULL;
volatile int max = 0;

int XCHG(volatile int *target, volatile int value)
{
    asm volatile("xchg (%0), %1\n": "+g" (target), "+&g" (value));
    return value;
}

enum { BUSY = -1 };

void wait_turn(int id)
{
    int local = XCHG(ready, BUSY);
    while (local != id) {
        // Oops, this wasn't our turn, wait!
        sched_yield();
        local = XCHG(ready, local);
    }
}

void pass_turn(int id)
{
    XCHG(ready, !id);
}

void child(int id)
{
    if (!fork()) {
        while (1) {
            wait_turn(id);
            int local = ++*cnt;
            if (!local || local > max) {
                *cnt = -1;
                pass_turn(id);
                exit(0);
            } else {
                printf("%d %d\n", id + 1, local);
                fflush(stdout);
                pass_turn(id);
            }
        }
    }
}

enum { P_RW = PROT_READ | PROT_WRITE };
enum { M_SA = MAP_SHARED | MAP_ANONYMOUS };

int main(int argc, char *argv[])
{
    sscanf(argv[1], "%d", &max);

    ready = mmap(NULL, sizeof(*ready), P_RW, M_SA, -1, 0);
    *ready = BUSY;

    cnt = mmap(NULL, sizeof(*cnt), P_RW, M_SA, -1, 0);
    *cnt = 0;

    child(0);
    child(1);

    pass_turn(1);
    while (wait(NULL) != -1 || errno != ECHILD); 
}
