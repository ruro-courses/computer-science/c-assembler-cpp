#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
    char name[PATH_MAX];
    const char *path = getenv("XDG_RUNTIME_DIR");
    if (!path) {
        path = getenv("TMPDIR");
    }
    if (!path) {
        path = "/tmp";
    }
    snprintf(name, sizeof(name), "%s/%u.py", path, getpid());

    FILE *file = fopen(name, "w");
    chmod(name, S_IRWXU);
    fprintf(file, "#!/usr/bin/env python3\n");
    fprintf(file, "import os\n");
    fprintf(file, "print(%s", *++argv);
    while (*++argv) {
        fprintf(file, "*%s", *argv);
    }
    fprintf(file, ")\n");
    fprintf(file, "os.remove(\"%s\")\n", name);
    fflush(file);
    fclose(file);
    execl(name, name, NULL);
}
