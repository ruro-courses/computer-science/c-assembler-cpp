#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>

void error(const char *msg)
{
    int errval = errno;
    fprintf(stderr, "%s\n", msg);
    if (errval) {
        fprintf(stderr, "Error: %s\n", strerror(errval));
    }
    exit(1);
}

char *skip_forward(char *ptr, long num, char *file_end)
{
    for (long i = 0; ptr < file_end && i < num; i++) {
        while (ptr < file_end && *ptr != '\n') {
            ptr++;
        }
        ptr++;
    }
    return ptr;
}

void print_backwards(char *ptr, char *lim)
{
    char *last = ptr;
    while (lim < ptr) {
        ptr--;
        while (lim < ptr && *(ptr - 1) != '\n') {
            ptr--;
        }
        printf("%.*s\n", (int)(last - ptr - 1), ptr);
        if (ptr == lim) {
            break;
        }
        last = ptr;
    }
}

enum { STRTOL_BASE = 10 };

long getarg(char *str)
{
    errno = 0;
    char *chk;
    long ret = strtol(str, &chk, STRTOL_BASE);
    if (*chk || errno) {
        error("Line should be a number.");
    }
    if (ret <= 0) {
        error("Line numbers should be greater than 0.");
    }
    ret--;
    return ret;
}

enum { PROT_RW = PROT_READ | PROT_WRITE };

int main(int argc, char *argv[])
{
    if (argc != 4)
        error("Usage: ./prog [FILENAME] [LINE_BEG] [LINE_END].");
    char *filename = argv[1];
    long line_beg = getarg(argv[2]);
    long line_end = getarg(argv[3]);
    int file = open(filename, O_RDONLY);
    if (file == -1) {
        error("Couldn't open file.");
    }
    struct stat fileinfo;
    if (lstat(filename, &fileinfo)) {
        error("Couldn't stat file.");
    }
    ssize_t filesize = fileinfo.st_size;
    if (filesize <= 0) {
        return 0;
    }
    char *contents = mmap(NULL, filesize, PROT_RW, MAP_PRIVATE, file, 0);
    if (contents == MAP_FAILED) {
        error("Couldn't mmap file.");
    }
    if (line_beg >= line_end) {
        return 0;
    }
    char *file_end = &contents[filesize];
    char *beg = skip_forward(contents, line_beg, file_end);
    char *end = skip_forward(contents, line_end, file_end);
    print_backwards(end, beg);
}
