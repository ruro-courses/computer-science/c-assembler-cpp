#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct
{
    int32_t key;
    int32_t left_idx;
    int32_t right_idx;
} Node;

void readNode(int fd, int32_t idx, Node *buf)
{
    ssize_t done, temp;
    if (lseek(fd, idx * sizeof(Node), SEEK_SET) < 0) { exit(-errno); }
    done = 0;
    while (sizeof(Node) - done) {
        temp = read(fd, buf + done, sizeof(Node) - done);
        if (temp < 0) {
            if (errno == EINTR) { continue;}
            exit(-errno);
        }
        done += temp;
    }
}

void traverseNode(int fd, int32_t idx)
{
    Node head;
    readNode(fd, idx, &head);
    if (head.right_idx) { traverseNode(fd, head.right_idx); }
    printf("%d\n", head.key);
    if (head.left_idx) { traverseNode(fd, head.left_idx); }
}

int main(int argc, char *argv[])
{
    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) { exit(-errno); }
    traverseNode(fd, 0);
}
