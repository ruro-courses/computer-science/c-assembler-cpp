﻿/*
 * Sorting Algorithms Comparison
 * (3 - 2 - 1 - 3)
 * Double, decreasing order (non-increasing), Bubble Sort and Shell Sort
 *
 * Author: s02160254 (Stotskii Andrey) Group 105
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>

//Range of doubles used in random array gen.
#define RANGE 100

//Swap elements in positions i and j.
#define swap(i, j)      tmp = arr[i];\
                        arr[i] = arr[j];\
                        arr[j] = tmp;\
                        swapcount++

//Check, if elements are in order (assuming i < j).
#define isOrd(i, j)    (++cmpcount && arr[i]>=arr[j])

void bubblesort(double *arr, long unsigned size) {
    long unsigned swapcount = 0, cmpcount = 0, end = size;
    bool unordered;
    double tmp = 0;
    long unsigned i;
    do {
        unordered = false;
        for (i = 1; i < end; i++) {
            if (!isOrd(i - 1, i)) {
                swap(i - 1, i);
                unordered = true;
            }
        }
        end--;
    } while (end && unordered);
    //Print statistics.
    printf("%s\t\t%lu\n%s\t%lu\n", "Element comparisons performed: ", cmpcount,
           "Element swap operations performed: ", swapcount);
    //Stop, if something went wrong and the array is not sorted.
    for (i = 1; i < size; i++) {
        assert(isOrd(i - 1, i));
    }
}

void shellsort(double *arr, long unsigned size) {
    long unsigned swapcount = 0, cmpcount = 0, gap = size / 2;
    double tmp = 0;
    long unsigned i, j;
    do {
        for (i = gap; i < size; i++)
            for (j = i; j >= gap && !isOrd(j - gap, j); j -= gap) {
                swap(j - gap, j);
            }
        gap /= 2;
    } while (gap);
    //Print statistics.
    printf("%s\t\t%lu\n%s\t%lu\n", "Element comparisons performed: ", cmpcount,
           "Element swap operations performed: ", swapcount);
    //Stop, if something went wrong and the array is not sorted.
    for (i = 1; i < size; i++) {
        assert(isOrd(i - 1, i));
    }
}

double *fillRandom(long unsigned size) {
    double *ret = NULL;
    //Stop, if unable to allocate enough memory
    assert(NULL != (ret = malloc(sizeof(double) * size)));
    long unsigned i;
    for (i = 0; i < size; i++) {
        //a random double, uniformly chosen from -RANGE to RANGE values
        ret[i] = 2 * (double) RANGE * (double) rand() / (double) RAND_MAX - RANGE;
    }
    return ret;
}

int cmpS(const void *a, const void *b) {
    return (*(double *) a) >= (*(double *) b) ? -1 : 1;
}

int cmpR(const void *a, const void *b) {
    return (*(double *) a) >= (*(double *) b) ? 1 : -1;
}

int main(void) {
    long unsigned N;
    unsigned seed;
    double *straight, *reverse, *rand1, *rand2;
    printf("%s", "Please, input desired array size:");
    scanf("%lu", &N);

    do {
        seed = time(NULL);

        //Fill arrays, sort them in required order.
        srand(seed);
        straight = fillRandom(N);
        qsort(straight, N, sizeof(double), cmpS);
        reverse = fillRandom(N);
        qsort(reverse, N, sizeof(double), cmpR);
        rand1 = fillRandom(N);
        rand2 = fillRandom(N);

        //Shell Sort
        printf("\n\n%s\n", "Starting shell sort.");
        printf("\t\t%s\n", "Straight.");
        shellsort(straight, N);
        printf("\t\t%s\n", "Reverse.");
        shellsort(reverse, N);
        printf("\t\t%s\n", "Random #1.");
        shellsort(rand1, N);
        printf("\t\t%s\n", "Random #2.");
        shellsort(rand2, N);

        //Free
        free(straight);
        free(reverse);
        free(rand1);
        free(rand2);

        //Refill arrays, sort them in required order.
        //Because srand is reset to previous value,
        //The arrays will be the same for bubblesort and shellsort.
        srand(seed);
        straight = fillRandom(N);
        qsort(straight, N, sizeof(double), cmpS);
        reverse = fillRandom(N);
        qsort(reverse, N, sizeof(double), cmpR);
        rand1 = fillRandom(N);
        rand2 = fillRandom(N);

        //Bubble Sort
        printf("\n\n%s\n", "Starting bubble sort.");
        printf("\t\t%s\n", "Straight.");
        bubblesort(straight, N);
        printf("\t\t%s\n", "Reverse.");
        bubblesort(reverse, N);
        printf("\t\t%s\n", "Random #1.");
        bubblesort(rand1, N);
        printf("\t\t%s\n", "Random #2.");
        bubblesort(rand2, N);

        //Free
        free(straight);
        free(reverse);
        free(rand1);
        free(rand2);

        //Repeat?
        printf("%s", "Please, input new array size, or 0 to stop:");
        scanf("%lu", &N);
    } while (N);
    return 0;
}
